/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2011-2015  Regents of the University of California.
 *
 * This file is part of ndnSIM. See AUTHORS for complete list of ndnSIM authors and
 * contributors.
 *
 * ndnSIM is free software: you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * ndnSIM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ndnSIM, e.g., in COPYING.md file.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <cstdbool>
#include <cstdint>
#include <iostream>
#include <iterator>
#include <memory>
#include <sstream>
#include <string>
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/config-store-module.h"
#include "ns3/wifi-module.h"
#include "ns3/energy-module.h"
#include "ns3/ndnSIM-module.h"
#include "wsn-fw-strategy.hpp"

using namespace std;

namespace ns3 {
NS_LOG_COMPONENT_DEFINE("ndn.wifi");

//
// DISCLAIMER:  Note that this is an extremely simple example, containing just 2 wifi nodes
// communicating directly over AdHoc channel.
//
uint32_t MacTxDropCount, PhyTxDropCount, PhyRxDropCount;
NodeContainer nodes;
std::string folder = "/64/1/1/";
std::vector<ApplicationContainer> c_mer;
uint32_t sinkid;
bool depleted = false;
shared_ptr<ndn::NetDeviceFace> MyNetDeviceFaceCallback(Ptr<Node> node,
		Ptr<ndn::L3Protocol> ndn, Ptr<NetDevice> device) {
	NS_LOG_DEBUG("Create custom network device " << node->GetId ());
	shared_ptr<ndn::NetDeviceFace> face = std::make_shared<ndn::NetDeviceFace>(
			node, device);
	face->setDescription(" THE DEVIL FACE 3:) ");
	ndn->addFace(face);
	NS_LOG_INFO(
			"Node " << node->GetId() << ": added NetDeviceFace as face #" << face->getId());
	ns3::ndn::FibHelper::AddRoute(node, "/", face, 0);
	return face;
}

template<uint32_t node> void RemainingEnergyTrace(double oldValue,
		double newValue) {
	nodes.Get(node)->SetRemainingEnergy(newValue);
	if (newValue <= 0.04) {
		std::stringstream ss;
		ss << "results/" + folder + "network_life_" << node << ".log";
		static std::fstream f(ss.str().c_str(), std::ios::out);
		f << Simulator::Now().GetSeconds() << "\t" << node << std::endl;
		std::stringstream xx;
		xx << "results/" + folder + "timeout_" << sinkid << ".log";
		static std::fstream x(xx.str().c_str(), std::ios::out);
		x << Simulator::Now().GetSeconds() << "\t" << nodes.Get(sinkid)->GetNTieout() << std::endl;

		Simulator::Stop();

	}
//	std::stringstream ss;
//	ss << "results/energy_" << node << ".log";
//
//	static std::fstream f(ss.str().c_str(), std::ios::out);
//
//	// NS_LOG_INFO(Simulator::Now().GetSeconds() << "    remaining energy@ node " << node <<"= " << newValue);
//	f << Simulator::Now().GetSeconds() << "    remaining energy=" << newValue
//			<< std::endl;
}
template<uint32_t node> void TotalEnergyConsumptionTrace(double oldValue,
		double newValue) {
/*	std::stringstream ss;
	ss << "results" + folder + "energy_" << node << ".log";

	static std::fstream f(ss.str().c_str(), std::ios::out);

	f << Simulator::Now().GetSeconds() << "\t" << node << "\t" << newValue
			<< std::endl;*/
}
void MacTxDrop(Ptr<const Packet> p) {
	NS_LOG_INFO("Packet Drop");
	MacTxDropCount++;
}

void PrintDrop() {
	std::cout << Simulator::Now().GetSeconds() << "\t" << MacTxDropCount << "\t"
			<< PhyTxDropCount << "\t" << PhyRxDropCount << "\n";
	Simulator::Schedule(Seconds(1.0), &PrintDrop);
}

void PhyTxDrop(Ptr<const Packet> p) {
	NS_LOG_INFO("Packet Drop");
	PhyTxDropCount++;
}
void PhyRxDrop(Ptr<const Packet> p) {
	NS_LOG_INFO("Packet Drop");
	PhyRxDropCount++;
}
void testNDP(Ptr<Node> n) {
	NS_LOG_DEBUG("Position Changed");
	ns3::Ptr<ns3::MobilityModel> mob = n->GetObject<ns3::MobilityModel>();
	ns3::Vector poss = mob->GetPosition();
	if (n->GetId() == 2)
		mob->SetPosition(Vector(200, poss.y, 0));
	else
		mob->SetPosition(Vector(900, 100, 0));
}
int main(int argc, char* argv[]) {
	bool verbose = false;

	//////////////////////
	//////////////////////
	//////////////////////

	std::string phyMode("DsssRate2Mbps");
	double distance = 50;  // m
	uint32_t nNodes = 64;
	uint32_t numTask = 4; // seconds
	uint32_t packetSize = 100; // bytes
	std::string rtslimit = "1500";


	double initialEnergy = 5; // joule
	double idleCurrent = 0; // Ampere
	double txCurrent = 0.185; // Ampere
	double rxCurrent = 0.04; // Ampere
	double voltage = 3.0; // volts
	double txPowerStart = 0.0; // dbm
	double txPowerEnd = 10.0; // dbm
	uint32_t nTxPowerLevels = 10;
	//uint32_t txPowerLevel = 0;

	CommandLine cmd;

	cmd.AddValue("phyMode", "Wifi Phy mode", phyMode);
	cmd.AddValue("distance", "distance (m)", distance);
	cmd.AddValue("packetSize", "Payload Size", packetSize);
	//cmd.AddValue("rtslimit", "RTS/CTS Threshold (bytes)", rtslimit);
	cmd.AddValue("nNodes", "Number of Nodes", nNodes);
	cmd.AddValue("fld", "folder to save log files", folder);
	cmd.AddValue("numtask", "number of tasks", numTask);

	cmd.Parse(argc, argv);

	if (verbose) {
		LogComponentEnable("ndn.wifi", LOG_LEVEL_ALL);
		LogComponentEnable("ndn-cxx.Face", LOG_LEVEL_ALL);
		LogComponentEnable("ndn.App", LOG_LEVEL_ALL);
		LogComponentEnable("ndn.ConsumerPush", LOG_LEVEL_ALL);
		LogComponentEnable("ndn.FibHelper", LOG_LEVEL_ALL);
		LogComponentEnable("ndn.L3Protocol", LOG_LEVEL_ALL);
		LogComponentEnable("ndn.NetDeviceFace", LOG_LEVEL_ALL);
		LogComponentEnable("ndn.Sink", LOG_LEVEL_ALL);
		LogComponentEnable("nfd.FaceManager", LOG_LEVEL_ALL);
		LogComponentEnable("nfd.FaceTable", LOG_LEVEL_ALL);
		LogComponentEnable("nfd.Forwarder", LOG_LEVEL_ALL);
		LogComponentEnable("nfd.NameTree", LOG_LEVEL_ALL);
		LogComponentEnable("nfd.WsnStrategy", LOG_LEVEL_ALL);
		LogComponentEnable("nfd.ContentStore", LOG_LEVEL_ALL);
	}

	// Convert to time object

	// turn off RTS/CTS for frames below 2200 bytes
	Config::SetDefault("ns3::WifiRemoteStationManager::RtsCtsThreshold",
			StringValue(rtslimit));
	// Fix non-unicast data rate to be the same as that of unicast
	Config::SetDefault("ns3::WifiRemoteStationManager::NonUnicastMode",
			StringValue(phyMode));

	nodes.Create(nNodes);

	// The below set of helpers will help us to put together the wifi NICs we want
	WifiHelper wifi;

	YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default();
	// set it to zero; otherwise, gain will be added
	wifiPhy.Set("RxGain", DoubleValue(-10));
	// ns-3 supports RadioTap and Prism tracing extensions for 802.11b
	wifiPhy.SetPcapDataLinkType(YansWifiPhyHelper::DLT_IEEE802_11_RADIO);
	wifiPhy.Set("TxPowerStart", DoubleValue(txPowerStart));
	wifiPhy.Set("TxPowerEnd", DoubleValue(txPowerEnd));
	wifiPhy.Set("TxPowerLevels", UintegerValue(nTxPowerLevels));
	YansWifiChannelHelper wifiChannel;

	wifiChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
	wifiChannel.AddPropagationLoss("ns3::RangePropagationLossModel", "MaxRange",
			DoubleValue(100.0));
	wifiPhy.SetChannel(wifiChannel.Create());

	// Add a non-QoS upper mac, and disable rate control
	NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default();
	wifi.SetStandard(WIFI_PHY_STANDARD_80211g);
	wifi.SetRemoteStationManager("ns3::ConstantRateWifiManager", "DataMode",
			StringValue(phyMode), "ControlMode", StringValue(phyMode));
	// Set it to adhoc mode
	wifiMac.SetType("ns3::AdhocWifiMac");
	NetDeviceContainer devices = wifi.Install(wifiPhy, wifiMac, nodes);

	/** mobility **/
	MobilityHelper mobility;
//	Ptr<ListPositionAllocator> positionAlloc = CreateObject<
//			ListPositionAllocator>();
//	uint32_t x = 250, x1 = 0; //250 range
//	for (uint32_t i = 1; i <= nNodes; i++) {
//		positionAlloc->Add(Vector(x1, 0, 0.0));
//		x1 += x;
//
//	}
//	mobility.SetPositionAllocator(positionAlloc);
//	mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");

	mobility.SetPositionAllocator("ns3::GridPositionAllocator", "MinX",
			DoubleValue(0.0), "MinY", DoubleValue(0.0), "DeltaX",
			DoubleValue(distance), "DeltaY", DoubleValue(distance), "GridWidth",
			UintegerValue(sqrt(nNodes)), "LayoutType", StringValue("RowFirst"));
	mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	// 2. Install Mobility model
	mobility.Install(nodes);
	/*	ns3::Ptr<ns3::MobilityModel> mob = nodes.Get(2)->GetObject<
	 ns3::MobilityModel>();
	 ns3::Vector poss = mob->GetPosition();
	 mob->SetPosition(Vector(900,poss.y,0));
	 Simulator::Schedule(Seconds(125.0),&testNDP,nodes.Get(2));
	 //	mob = nodes.Get(6)->GetObject<
	 //					ns3::MobilityModel>();
	 //	poss = mob->GetPosition();
	 //		mob->SetPosition(Vector(poss.x,900,0));
	 Simulator::Schedule(Seconds(190.0),&testNDP,nodes.Get(6));*/

	if (nNodes == 64)
		sinkid = 36;
	if (nNodes == 81)
		sinkid = 40;
	if (nNodes == 100)
		sinkid = 45;
	if (nNodes == 121)
		sinkid = 60;
	//WifiHelper::EnableLogComponents();

	EnergySourceContainer eSources;
	BasicEnergySourceHelper basicSourceHelper;
	WifiRadioEnergyModelHelper radioEnergyHelper;
	basicSourceHelper.Set("BasicEnergySourceInitialEnergyJ",
			DoubleValue(initialEnergy));
	basicSourceHelper.Set("BasicEnergySupplyVoltageV", DoubleValue(voltage));

	radioEnergyHelper.Set("IdleCurrentA", DoubleValue(idleCurrent));
	radioEnergyHelper.Set("TxCurrentA", DoubleValue(txCurrent));
	radioEnergyHelper.Set("RxCurrentA", DoubleValue(rxCurrent));
	radioEnergyHelper.Set("CcaBusyCurrentA", DoubleValue(0.0));
	radioEnergyHelper.Set("SwitchingCurrentA", DoubleValue(0.0));

	// compute the efficiency of the power amplifier (eta) assuming that the provided value for tx current
	// corresponds to the minimum tx power level
	double eta = WifiTxCurrentModel::DbmToW(txPowerStart)
			/ ((txCurrent - idleCurrent) * voltage);

	radioEnergyHelper.SetTxCurrentModel("ns3::LinearWifiTxCurrentModel",
			"Voltage", DoubleValue(voltage), "IdleCurrent",
			DoubleValue(idleCurrent), "Eta", DoubleValue(eta));
	// install an energy source on each node
	for (NodeContainer::Iterator n = nodes.Begin(); n != nodes.End(); n++) {
		if ((*n)->GetId() != sinkid) {
			eSources.Add(basicSourceHelper.Install(*n));

			Ptr<WifiNetDevice> wnd;

			for (uint32_t i = 0; i < (*n)->GetNDevices(); ++i) {
				wnd = (*n)->GetDevice(i)->GetObject<WifiNetDevice>();
				// if it is a WifiNetDevice
				if (wnd != 0) {
					// this device draws power from the last created energy source
					radioEnergyHelper.Install(wnd,
							eSources.Get(eSources.GetN() - 1));
				}
			}
		}
	}

	// 3. Install NDN stack
	NS_LOG_INFO("Installing NDN stack");
	ndn::StackHelper ndnHelper;
	ndnHelper.AddNetDeviceFaceCreateCallback(WifiNetDevice::GetTypeId(),
			MakeCallback(MyNetDeviceFaceCallback));
	//ndnHelper.SetOldContentStore("ns3::ndn::cs::Fifo", "MaxSize", "10000");
	ndnHelper.SetOldContentStore("ns3::ndn::cs::Nocache");

	ndnHelper.SetDefaultRoutes(true);
	ndnHelper.Install(nodes);
	ndn::StrategyChoiceHelper::Install<nfd::fw::WsnStrategy>(nodes, "/");

	// 4. Set up applications
	NS_LOG_INFO("Installing Applications");

	ndn::AppHelper consumerHelper("SConsumerCbr");
	//ndn::AppHelper consumerHelper("ns3::ndn::ConsumerBatches");
	consumerHelper.SetAttribute("Frequency", StringValue("1"));
	//consumerHelper.SetAttribute("Batches", StringValue("1s 1 5s 5 10s 10"));
	// ApplicationContainer Sink1 = consumerHelper.Install(nodes.Get(2));
	for (uint32_t i = 0; i < nNodes; i++) {
		consumerHelper.SetPrefixP("/room" + std::to_string(i));
		consumerHelper.SetPrefix("/" + std::to_string(numTask));
		ApplicationContainer app = consumerHelper.Install(nodes.Get(i));
		app.Start(Seconds(0));
		c_mer.push_back(app);
	}

	ndn::AppHelper producerHelper("Sink");
	//ndn::AppHelper producerHelper("ns3::ndn::Producer");
	//producerHelper.SetAttribute("PayloadSize", StringValue("100"));
	// producerHelper.SetPrefix("/ok");
	//producerHelper.Install(nodes.Get(1));

	switch (numTask) {
	case 4: {
		for (uint32_t i = 0; i < nNodes; i++) {
			if (nNodes == 64)
				if (i == 0 || i == 7 || i == 56 || i == 63) {
					producerHelper.SetPrefix("/room" + std::to_string(i));
					producerHelper.Install(nodes.Get(i));
				}
			if (nNodes == 81)
				if (i == 0 || i == 8 || i == 72 || i == 80) {
					producerHelper.SetPrefix("/room" + std::to_string(i));
					producerHelper.Install(nodes.Get(i));
				}
			if (nNodes == 100)
				if (i == 0 || i == 9 || i == 90 || i == 99) {
					producerHelper.SetPrefix("/room" + std::to_string(i));
					producerHelper.Install(nodes.Get(i));
				}
			if (nNodes == 121)
				if (i == 0 || i == 10 || i == 110 || i == 120) {
					producerHelper.SetPrefix("/room" + std::to_string(i));
					producerHelper.Install(nodes.Get(i));
				}

		}
	}
		break;

	case 8: {

		for (uint32_t i = 0; i < nNodes; i++) {
			if (nNodes == 64)
				if (i == 0 || i == 7 || i == 56 || i == 63 || i == 4 || i == 31
						|| i == 24 || i == 60) {
					producerHelper.SetPrefix("/room" + std::to_string(i));
					producerHelper.Install(nodes.Get(i));
				}
			if (nNodes == 81)
				if (i == 0 || i == 8 || i == 72 || i == 80 || i == 76 || i == 36
						|| i == 44 || i == 4) {
					producerHelper.SetPrefix("/room" + std::to_string(i));
					producerHelper.Install(nodes.Get(i));
				}
			if (nNodes == 100)
				if (i == 0 || i == 9 || i == 90 || i == 99 || i == 5 || i == 40
						|| i == 49 || i == 95) {
					producerHelper.SetPrefix("/room" + std::to_string(i));
					producerHelper.Install(nodes.Get(i));
				}
			if (nNodes == 121)
				if (i == 0 || i == 10 || i == 110 || i == 120 || i == 5
						|| i == 55 || i == 65 || i == 115) {
					producerHelper.SetPrefix("/room" + std::to_string(i));
					producerHelper.Install(nodes.Get(i));
				}
		}
	}
		break;
	case 12: {
		for (uint32_t i = 0; i < nNodes; i++) {
			if (nNodes == 64)
				if (i == 0 || i == 7 || i == 56 || i == 63 || i == 4 || i == 31
						|| i == 24 || i == 60 || i == 3 || i == 39 || i == 32
						|| i == 59) {
					producerHelper.SetPrefix("/room" + std::to_string(i));
					producerHelper.Install(nodes.Get(i));
				}
			if (nNodes == 81)
				if (i == 0 || i == 8 || i == 72 || i == 80 || i == 76 || i == 36
						|| i == 44 || i == 4 || i == 75 || i == 27 || i == 35
						|| i == 3) {
					producerHelper.SetPrefix("/room" + std::to_string(i));
					producerHelper.Install(nodes.Get(i));
				}
			if (nNodes == 100)
				if (i == 0 || i == 9 || i == 90 || i == 99 || i == 5 || i == 40
						|| i == 49 || i == 95 || i == 4 || i == 30 || i == 39
						|| i == 94) {
					producerHelper.SetPrefix("/room" + std::to_string(i));
					producerHelper.Install(nodes.Get(i));
				}
			if (nNodes == 121)
				if (i == 0 || i == 10 || i == 110 || i == 120 || i == 5
						|| i == 55 || i == 65 || i == 115 || i == 4 || i == 44
						|| i == 54 || i == 114) {
					producerHelper.SetPrefix("/room" + std::to_string(i));
					producerHelper.Install(nodes.Get(i));
				}
		}
	}
		break;
	case 16: {
		for (uint32_t i = 0; i < nNodes; i++) {
			if (nNodes == 64)
				if (i == 0 || i == 7 || i == 56 || i == 63 || i == 4 || i == 31
						|| i == 24 || i == 60 || i == 3 || i == 39 || i == 32
						|| i == 59 || i == 5 || i == 23 || i == 16 || i == 61) {
					producerHelper.SetPrefix("/room" + std::to_string(i));
					producerHelper.Install(nodes.Get(i));
				}
			if (nNodes == 81)
				if (i == 0 || i == 8 || i == 72 || i == 80 || i == 76 || i == 36
						|| i == 44 || i == 4 || i == 75 || i == 27 || i == 35
						|| i == 3 || i == 77 || i == 45 || i == 53 || i == 5) {
					producerHelper.SetPrefix("/room" + std::to_string(i));
					producerHelper.Install(nodes.Get(i));
				}
			if (nNodes == 100)
				if (i == 0 || i == 9 || i == 90 || i == 99 || i == 5 || i == 40
						|| i == 49 || i == 95 || i == 4 || i == 30 || i == 39
						|| i == 94 || i == 6 || i == 50 || i == 59 || i == 96) {
					producerHelper.SetPrefix("/room" + std::to_string(i));
					producerHelper.Install(nodes.Get(i));
				}
			if (nNodes == 121)
				if (i == 0 || i == 10 || i == 110 || i == 120 || i == 5
						|| i == 55 || i == 65 || i == 115 || i == 4 || i == 44
						|| i == 54 || i == 114 || i == 6 || i == 66 || i == 76
						|| i == 116) {
					producerHelper.SetPrefix("/room" + std::to_string(i));
					producerHelper.Install(nodes.Get(i));
				}
		}
	}
		break;
	default: {
		producerHelper.SetPrefix("/room" + std::to_string(0));
		producerHelper.Install(nodes.Get(0));
	}
		break;
	}

	producerHelper.SetPrefix("/sink");
	producerHelper.Install(nodes.Get(sinkid));

	// Trace Collisions
//	Config::ConnectWithoutContext(
//			"/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/MacTxDrop",
//			MakeCallback(&MacTxDrop));
//	Config::ConnectWithoutContext(
//			"/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/PhyRxDrop",
//			MakeCallback(&PhyRxDrop));
//	Config::ConnectWithoutContext(
//			"/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/PhyTxDrop",
//			MakeCallback(&PhyTxDrop));

//
//	for(uint32_t i=0 ;i<nNodes;i++){
//		  eSources.Get (i)->TraceConnectWithoutContext ("RemainingEnergy", MakeCallback(&RemainingEnergyTrace<i>));
//	}

	eSources.Get(0)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<0>));
	eSources.Get(1)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<1>));
	eSources.Get(2)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<2>));
	eSources.Get(3)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<3>));
	eSources.Get(4)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<4>));
	eSources.Get(5)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<5>));
	eSources.Get(6)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<6>));
	eSources.Get(7)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<7>));
	eSources.Get(8)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<8>));
	eSources.Get(9)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<9>));
	eSources.Get(10)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<10>));
	eSources.Get(11)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<11>));
	eSources.Get(12)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<12>));
	eSources.Get(13)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<13>));
	eSources.Get(14)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<14>));
	eSources.Get(15)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<15>));
	eSources.Get(16)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<16>));
	eSources.Get(17)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<17>));
	eSources.Get(18)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<18>));
	eSources.Get(19)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<19>));
	eSources.Get(20)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<20>));
	eSources.Get(21)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<21>));
	eSources.Get(22)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<22>));
	eSources.Get(23)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<23>));
	eSources.Get(24)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<24>));
	eSources.Get(25)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<25>));
	eSources.Get(26)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<26>));
	eSources.Get(27)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<27>));
	eSources.Get(28)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<28>));
	eSources.Get(29)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<29>));
	eSources.Get(30)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<30>));
	eSources.Get(31)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<31>));
	eSources.Get(32)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<32>));
	eSources.Get(33)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<33>));
	eSources.Get(34)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<34>));
	eSources.Get(35)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<35>));
	if (sinkid != 36)
		eSources.Get(36)->TraceConnectWithoutContext("RemainingEnergy",
				MakeCallback(&RemainingEnergyTrace<36>));
	eSources.Get(37)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<37>));
	eSources.Get(38)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<38>));
	eSources.Get(39)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<39>));
	if (sinkid != 40)
		eSources.Get(40)->TraceConnectWithoutContext("RemainingEnergy",
				MakeCallback(&RemainingEnergyTrace<40>));
	eSources.Get(41)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<41>));
	eSources.Get(42)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<42>));
	eSources.Get(43)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<43>));
	eSources.Get(44)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<44>));
	if (sinkid != 45)
		eSources.Get(45)->TraceConnectWithoutContext("RemainingEnergy",
				MakeCallback(&RemainingEnergyTrace<45>));
	eSources.Get(46)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<46>));
	eSources.Get(47)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<47>));
	eSources.Get(48)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<48>));
	eSources.Get(49)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<49>));
	eSources.Get(50)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<50>));
	eSources.Get(51)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<51>));
	eSources.Get(52)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<52>));
	eSources.Get(53)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<53>));
	eSources.Get(54)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<54>));
	eSources.Get(55)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<55>));
	eSources.Get(56)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<56>));
	eSources.Get(57)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<57>));
	eSources.Get(58)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<58>));
	eSources.Get(59)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<59>));
	if (sinkid != 60)
		eSources.Get(60)->TraceConnectWithoutContext("RemainingEnergy",
				MakeCallback(&RemainingEnergyTrace<60>));
	eSources.Get(61)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<61>));
	eSources.Get(62)->TraceConnectWithoutContext("RemainingEnergy",
			MakeCallback(&RemainingEnergyTrace<62>));
	if (nNodes > 64) {
		eSources.Get(63)->TraceConnectWithoutContext("RemainingEnergy",
				MakeCallback(&RemainingEnergyTrace<63>));
		eSources.Get(64)->TraceConnectWithoutContext("RemainingEnergy",
				MakeCallback(&RemainingEnergyTrace<64>));
		eSources.Get(65)->TraceConnectWithoutContext("RemainingEnergy",
				MakeCallback(&RemainingEnergyTrace<65>));
		eSources.Get(66)->TraceConnectWithoutContext("RemainingEnergy",
				MakeCallback(&RemainingEnergyTrace<66>));
		eSources.Get(67)->TraceConnectWithoutContext("RemainingEnergy",
				MakeCallback(&RemainingEnergyTrace<67>));
		eSources.Get(68)->TraceConnectWithoutContext("RemainingEnergy",
				MakeCallback(&RemainingEnergyTrace<68>));
		eSources.Get(69)->TraceConnectWithoutContext("RemainingEnergy",
				MakeCallback(&RemainingEnergyTrace<69>));
		eSources.Get(70)->TraceConnectWithoutContext("RemainingEnergy",
				MakeCallback(&RemainingEnergyTrace<70>));
		eSources.Get(71)->TraceConnectWithoutContext("RemainingEnergy",
				MakeCallback(&RemainingEnergyTrace<71>));
		eSources.Get(72)->TraceConnectWithoutContext("RemainingEnergy",
				MakeCallback(&RemainingEnergyTrace<72>));
		eSources.Get(73)->TraceConnectWithoutContext("RemainingEnergy",
				MakeCallback(&RemainingEnergyTrace<73>));
		eSources.Get(74)->TraceConnectWithoutContext("RemainingEnergy",
				MakeCallback(&RemainingEnergyTrace<74>));
		eSources.Get(75)->TraceConnectWithoutContext("RemainingEnergy",
				MakeCallback(&RemainingEnergyTrace<75>));
		eSources.Get(76)->TraceConnectWithoutContext("RemainingEnergy",
				MakeCallback(&RemainingEnergyTrace<76>));
		eSources.Get(77)->TraceConnectWithoutContext("RemainingEnergy",
				MakeCallback(&RemainingEnergyTrace<77>));
		eSources.Get(78)->TraceConnectWithoutContext("RemainingEnergy",
				MakeCallback(&RemainingEnergyTrace<78>));
		eSources.Get(79)->TraceConnectWithoutContext("RemainingEnergy",
				MakeCallback(&RemainingEnergyTrace<79>));
		if (nNodes > 81) {
			eSources.Get(80)->TraceConnectWithoutContext("RemainingEnergy",
					MakeCallback(&RemainingEnergyTrace<80>));
			eSources.Get(81)->TraceConnectWithoutContext("RemainingEnergy",
					MakeCallback(&RemainingEnergyTrace<81>));
			eSources.Get(82)->TraceConnectWithoutContext("RemainingEnergy",
					MakeCallback(&RemainingEnergyTrace<82>));
			eSources.Get(83)->TraceConnectWithoutContext("RemainingEnergy",
					MakeCallback(&RemainingEnergyTrace<83>));
			eSources.Get(84)->TraceConnectWithoutContext("RemainingEnergy",
					MakeCallback(&RemainingEnergyTrace<84>));
			eSources.Get(85)->TraceConnectWithoutContext("RemainingEnergy",
					MakeCallback(&RemainingEnergyTrace<85>));
			eSources.Get(86)->TraceConnectWithoutContext("RemainingEnergy",
					MakeCallback(&RemainingEnergyTrace<86>));
			eSources.Get(87)->TraceConnectWithoutContext("RemainingEnergy",
					MakeCallback(&RemainingEnergyTrace<87>));
			eSources.Get(88)->TraceConnectWithoutContext("RemainingEnergy",
					MakeCallback(&RemainingEnergyTrace<88>));
			eSources.Get(89)->TraceConnectWithoutContext("RemainingEnergy",
					MakeCallback(&RemainingEnergyTrace<89>));
			eSources.Get(90)->TraceConnectWithoutContext("RemainingEnergy",
					MakeCallback(&RemainingEnergyTrace<90>));
			eSources.Get(91)->TraceConnectWithoutContext("RemainingEnergy",
					MakeCallback(&RemainingEnergyTrace<91>));
			eSources.Get(92)->TraceConnectWithoutContext("RemainingEnergy",
					MakeCallback(&RemainingEnergyTrace<92>));
			eSources.Get(93)->TraceConnectWithoutContext("RemainingEnergy",
					MakeCallback(&RemainingEnergyTrace<93>));
			eSources.Get(94)->TraceConnectWithoutContext("RemainingEnergy",
					MakeCallback(&RemainingEnergyTrace<94>));
			eSources.Get(95)->TraceConnectWithoutContext("RemainingEnergy",
					MakeCallback(&RemainingEnergyTrace<95>));
			eSources.Get(96)->TraceConnectWithoutContext("RemainingEnergy",
					MakeCallback(&RemainingEnergyTrace<96>));
			eSources.Get(97)->TraceConnectWithoutContext("RemainingEnergy",
					MakeCallback(&RemainingEnergyTrace<97>));
			eSources.Get(98)->TraceConnectWithoutContext("RemainingEnergy",
					MakeCallback(&RemainingEnergyTrace<98>));
			if (nNodes > 100) {
				eSources.Get(99)->TraceConnectWithoutContext("RemainingEnergy",
						MakeCallback(&RemainingEnergyTrace<99>));
				eSources.Get(100)->TraceConnectWithoutContext("RemainingEnergy",
						MakeCallback(&RemainingEnergyTrace<100>));
				eSources.Get(101)->TraceConnectWithoutContext("RemainingEnergy",
						MakeCallback(&RemainingEnergyTrace<101>));
				eSources.Get(102)->TraceConnectWithoutContext("RemainingEnergy",
						MakeCallback(&RemainingEnergyTrace<102>));
				eSources.Get(103)->TraceConnectWithoutContext("RemainingEnergy",
						MakeCallback(&RemainingEnergyTrace<103>));
				eSources.Get(104)->TraceConnectWithoutContext("RemainingEnergy",
						MakeCallback(&RemainingEnergyTrace<104>));
				eSources.Get(105)->TraceConnectWithoutContext("RemainingEnergy",
						MakeCallback(&RemainingEnergyTrace<105>));
				eSources.Get(106)->TraceConnectWithoutContext("RemainingEnergy",
						MakeCallback(&RemainingEnergyTrace<106>));
				eSources.Get(107)->TraceConnectWithoutContext("RemainingEnergy",
						MakeCallback(&RemainingEnergyTrace<107>));
				eSources.Get(108)->TraceConnectWithoutContext("RemainingEnergy",
						MakeCallback(&RemainingEnergyTrace<108>));
				eSources.Get(109)->TraceConnectWithoutContext("RemainingEnergy",
						MakeCallback(&RemainingEnergyTrace<109>));
				eSources.Get(110)->TraceConnectWithoutContext("RemainingEnergy",
						MakeCallback(&RemainingEnergyTrace<110>));
				eSources.Get(111)->TraceConnectWithoutContext("RemainingEnergy",
						MakeCallback(&RemainingEnergyTrace<111>));
				eSources.Get(112)->TraceConnectWithoutContext("RemainingEnergy",
						MakeCallback(&RemainingEnergyTrace<112>));
				eSources.Get(113)->TraceConnectWithoutContext("RemainingEnergy",
						MakeCallback(&RemainingEnergyTrace<113>));
				eSources.Get(114)->TraceConnectWithoutContext("RemainingEnergy",
						MakeCallback(&RemainingEnergyTrace<114>));
				eSources.Get(115)->TraceConnectWithoutContext("RemainingEnergy",
						MakeCallback(&RemainingEnergyTrace<115>));
				eSources.Get(116)->TraceConnectWithoutContext("RemainingEnergy",
						MakeCallback(&RemainingEnergyTrace<116>));
				eSources.Get(117)->TraceConnectWithoutContext("RemainingEnergy",
						MakeCallback(&RemainingEnergyTrace<117>));
				eSources.Get(118)->TraceConnectWithoutContext("RemainingEnergy",
						MakeCallback(&RemainingEnergyTrace<118>));
				eSources.Get(119)->TraceConnectWithoutContext("RemainingEnergy",
						MakeCallback(&RemainingEnergyTrace<119>));
			}
		}
	}
	nodes.Get(sinkid)->SetRemainingEnergy(0.0);

	eSources.Get(0)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<0>));
	eSources.Get(1)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<1>));
	eSources.Get(2)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<2>));
	eSources.Get(3)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<3>));
	eSources.Get(4)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<4>));
	eSources.Get(5)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<5>));
	eSources.Get(6)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<6>));
	eSources.Get(7)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<7>));
	eSources.Get(8)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<8>));
	eSources.Get(9)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<9>));
	eSources.Get(10)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<10>));
	eSources.Get(11)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<11>));
	eSources.Get(12)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<12>));
	eSources.Get(13)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<13>));
	eSources.Get(14)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<14>));
	eSources.Get(15)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<15>));
	eSources.Get(16)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<16>));
	eSources.Get(17)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<17>));
	eSources.Get(18)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<18>));
	eSources.Get(19)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<19>));
	eSources.Get(20)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<20>));
	eSources.Get(21)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<21>));
	eSources.Get(22)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<22>));
	eSources.Get(23)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<23>));
	eSources.Get(24)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<24>));
	eSources.Get(25)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<25>));
	eSources.Get(26)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<26>));
	eSources.Get(27)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<27>));
	eSources.Get(28)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<28>));
	eSources.Get(29)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<29>));
	eSources.Get(30)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<30>));
	eSources.Get(31)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<31>));
	eSources.Get(32)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<32>));
	eSources.Get(33)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<33>));
	eSources.Get(34)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<34>));
	eSources.Get(35)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<35>));

	if (sinkid != 36)
		eSources.Get(36)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(
				0)->TraceConnectWithoutContext("TotalEnergyConsumption",
				MakeCallback(&TotalEnergyConsumptionTrace<36>));
	eSources.Get(37)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<37>));
	eSources.Get(38)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<38>));
	eSources.Get(39)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<39>));
	if (sinkid != 40)
		eSources.Get(40)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(
				0)->TraceConnectWithoutContext("TotalEnergyConsumption",
				MakeCallback(&TotalEnergyConsumptionTrace<40>));
	eSources.Get(41)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<41>));
	eSources.Get(42)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<42>));
	eSources.Get(43)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<43>));
	eSources.Get(44)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<44>));
	if (sinkid != 45)
		eSources.Get(45)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(
				0)->TraceConnectWithoutContext("TotalEnergyConsumption",
				MakeCallback(&TotalEnergyConsumptionTrace<45>));
	eSources.Get(46)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<46>));
	eSources.Get(47)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<47>));
	eSources.Get(48)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<48>));
	eSources.Get(49)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<49>));
	eSources.Get(50)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<50>));
	eSources.Get(51)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<51>));
	eSources.Get(52)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<52>));
	eSources.Get(53)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<53>));
	eSources.Get(54)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<54>));
	eSources.Get(55)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<55>));
	eSources.Get(56)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<56>));
	eSources.Get(57)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<57>));
	eSources.Get(58)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<58>));
	eSources.Get(59)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<59>));
	if (sinkid != 60)
		eSources.Get(60)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(
				0)->TraceConnectWithoutContext("TotalEnergyConsumption",
				MakeCallback(&TotalEnergyConsumptionTrace<60>));
	eSources.Get(61)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<61>));

	eSources.Get(62)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
			"TotalEnergyConsumption",
			MakeCallback(&TotalEnergyConsumptionTrace<62>));
	if (nNodes > 64) {
		eSources.Get(63)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(
				0)->TraceConnectWithoutContext("TotalEnergyConsumption",
				MakeCallback(&TotalEnergyConsumptionTrace<63>));
		eSources.Get(64)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(
				0)->TraceConnectWithoutContext("TotalEnergyConsumption",
				MakeCallback(&TotalEnergyConsumptionTrace<64>));
		eSources.Get(65)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(
				0)->TraceConnectWithoutContext("TotalEnergyConsumption",
				MakeCallback(&TotalEnergyConsumptionTrace<65>));
		eSources.Get(66)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(
				0)->TraceConnectWithoutContext("TotalEnergyConsumption",
				MakeCallback(&TotalEnergyConsumptionTrace<66>));
		eSources.Get(67)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(
				0)->TraceConnectWithoutContext("TotalEnergyConsumption",
				MakeCallback(&TotalEnergyConsumptionTrace<67>));
		eSources.Get(68)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(
				0)->TraceConnectWithoutContext("TotalEnergyConsumption",
				MakeCallback(&TotalEnergyConsumptionTrace<68>));
		eSources.Get(69)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(
				0)->TraceConnectWithoutContext("TotalEnergyConsumption",
				MakeCallback(&TotalEnergyConsumptionTrace<69>));
		eSources.Get(70)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(
				0)->TraceConnectWithoutContext("TotalEnergyConsumption",
				MakeCallback(&TotalEnergyConsumptionTrace<70>));
		eSources.Get(71)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(
				0)->TraceConnectWithoutContext("TotalEnergyConsumption",
				MakeCallback(&TotalEnergyConsumptionTrace<71>));
		eSources.Get(72)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(
				0)->TraceConnectWithoutContext("TotalEnergyConsumption",
				MakeCallback(&TotalEnergyConsumptionTrace<72>));
		eSources.Get(73)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(
				0)->TraceConnectWithoutContext("TotalEnergyConsumption",
				MakeCallback(&TotalEnergyConsumptionTrace<73>));
		eSources.Get(74)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(
				0)->TraceConnectWithoutContext("TotalEnergyConsumption",
				MakeCallback(&TotalEnergyConsumptionTrace<74>));
		eSources.Get(75)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(
				0)->TraceConnectWithoutContext("TotalEnergyConsumption",
				MakeCallback(&TotalEnergyConsumptionTrace<75>));
		eSources.Get(76)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(
				0)->TraceConnectWithoutContext("TotalEnergyConsumption",
				MakeCallback(&TotalEnergyConsumptionTrace<76>));
		eSources.Get(77)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(
				0)->TraceConnectWithoutContext("TotalEnergyConsumption",
				MakeCallback(&TotalEnergyConsumptionTrace<77>));
		eSources.Get(78)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(
				0)->TraceConnectWithoutContext("TotalEnergyConsumption",
				MakeCallback(&TotalEnergyConsumptionTrace<78>));

		eSources.Get(79)->FindDeviceEnergyModels("ns3::WifiRadioEnergyModel").Get(
				0)->TraceConnectWithoutContext("TotalEnergyConsumption",
				MakeCallback(&TotalEnergyConsumptionTrace<79>));
		if (nNodes > 81) {
			eSources.Get(80)->FindDeviceEnergyModels(
					"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
					"TotalEnergyConsumption",
					MakeCallback(&TotalEnergyConsumptionTrace<80>));
			eSources.Get(81)->FindDeviceEnergyModels(
					"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
					"TotalEnergyConsumption",
					MakeCallback(&TotalEnergyConsumptionTrace<81>));
			eSources.Get(82)->FindDeviceEnergyModels(
					"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
					"TotalEnergyConsumption",
					MakeCallback(&TotalEnergyConsumptionTrace<82>));
			eSources.Get(83)->FindDeviceEnergyModels(
					"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
					"TotalEnergyConsumption",
					MakeCallback(&TotalEnergyConsumptionTrace<83>));
			eSources.Get(84)->FindDeviceEnergyModels(
					"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
					"TotalEnergyConsumption",
					MakeCallback(&TotalEnergyConsumptionTrace<84>));
			eSources.Get(85)->FindDeviceEnergyModels(
					"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
					"TotalEnergyConsumption",
					MakeCallback(&TotalEnergyConsumptionTrace<85>));
			eSources.Get(86)->FindDeviceEnergyModels(
					"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
					"TotalEnergyConsumption",
					MakeCallback(&TotalEnergyConsumptionTrace<86>));
			eSources.Get(87)->FindDeviceEnergyModels(
					"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
					"TotalEnergyConsumption",
					MakeCallback(&TotalEnergyConsumptionTrace<87>));
			eSources.Get(88)->FindDeviceEnergyModels(
					"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
					"TotalEnergyConsumption",
					MakeCallback(&TotalEnergyConsumptionTrace<88>));
			eSources.Get(89)->FindDeviceEnergyModels(
					"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
					"TotalEnergyConsumption",
					MakeCallback(&TotalEnergyConsumptionTrace<89>));
			eSources.Get(90)->FindDeviceEnergyModels(
					"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
					"TotalEnergyConsumption",
					MakeCallback(&TotalEnergyConsumptionTrace<90>));
			eSources.Get(91)->FindDeviceEnergyModels(
					"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
					"TotalEnergyConsumption",
					MakeCallback(&TotalEnergyConsumptionTrace<91>));
			eSources.Get(92)->FindDeviceEnergyModels(
					"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
					"TotalEnergyConsumption",
					MakeCallback(&TotalEnergyConsumptionTrace<92>));
			eSources.Get(93)->FindDeviceEnergyModels(
					"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
					"TotalEnergyConsumption",
					MakeCallback(&TotalEnergyConsumptionTrace<93>));
			eSources.Get(94)->FindDeviceEnergyModels(
					"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
					"TotalEnergyConsumption",
					MakeCallback(&TotalEnergyConsumptionTrace<94>));
			eSources.Get(95)->FindDeviceEnergyModels(
					"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
					"TotalEnergyConsumption",
					MakeCallback(&TotalEnergyConsumptionTrace<95>));
			eSources.Get(96)->FindDeviceEnergyModels(
					"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
					"TotalEnergyConsumption",
					MakeCallback(&TotalEnergyConsumptionTrace<96>));
			eSources.Get(97)->FindDeviceEnergyModels(
					"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
					"TotalEnergyConsumption",
					MakeCallback(&TotalEnergyConsumptionTrace<97>));
			eSources.Get(98)->FindDeviceEnergyModels(
					"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
					"TotalEnergyConsumption",
					MakeCallback(&TotalEnergyConsumptionTrace<98>));
			if (nNodes > 100) {
				eSources.Get(99)->FindDeviceEnergyModels(
						"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
						"TotalEnergyConsumption",
						MakeCallback(&TotalEnergyConsumptionTrace<99>));
				eSources.Get(100)->FindDeviceEnergyModels(
						"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
						"TotalEnergyConsumption",
						MakeCallback(&TotalEnergyConsumptionTrace<100>));
				eSources.Get(101)->FindDeviceEnergyModels(
						"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
						"TotalEnergyConsumption",
						MakeCallback(&TotalEnergyConsumptionTrace<101>));
				eSources.Get(102)->FindDeviceEnergyModels(
						"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
						"TotalEnergyConsumption",
						MakeCallback(&TotalEnergyConsumptionTrace<102>));
				eSources.Get(103)->FindDeviceEnergyModels(
						"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
						"TotalEnergyConsumption",
						MakeCallback(&TotalEnergyConsumptionTrace<103>));
				eSources.Get(104)->FindDeviceEnergyModels(
						"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
						"TotalEnergyConsumption",
						MakeCallback(&TotalEnergyConsumptionTrace<104>));
				eSources.Get(105)->FindDeviceEnergyModels(
						"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
						"TotalEnergyConsumption",
						MakeCallback(&TotalEnergyConsumptionTrace<105>));
				eSources.Get(106)->FindDeviceEnergyModels(
						"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
						"TotalEnergyConsumption",
						MakeCallback(&TotalEnergyConsumptionTrace<106>));
				eSources.Get(107)->FindDeviceEnergyModels(
						"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
						"TotalEnergyConsumption",
						MakeCallback(&TotalEnergyConsumptionTrace<107>));
				eSources.Get(108)->FindDeviceEnergyModels(
						"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
						"TotalEnergyConsumption",
						MakeCallback(&TotalEnergyConsumptionTrace<108>));
				eSources.Get(109)->FindDeviceEnergyModels(
						"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
						"TotalEnergyConsumption",
						MakeCallback(&TotalEnergyConsumptionTrace<109>));
				eSources.Get(110)->FindDeviceEnergyModels(
						"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
						"TotalEnergyConsumption",
						MakeCallback(&TotalEnergyConsumptionTrace<110>));
				eSources.Get(111)->FindDeviceEnergyModels(
						"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
						"TotalEnergyConsumption",
						MakeCallback(&TotalEnergyConsumptionTrace<111>));
				eSources.Get(112)->FindDeviceEnergyModels(
						"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
						"TotalEnergyConsumption",
						MakeCallback(&TotalEnergyConsumptionTrace<112>));
				eSources.Get(113)->FindDeviceEnergyModels(
						"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
						"TotalEnergyConsumption",
						MakeCallback(&TotalEnergyConsumptionTrace<113>));
				eSources.Get(114)->FindDeviceEnergyModels(
						"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
						"TotalEnergyConsumption",
						MakeCallback(&TotalEnergyConsumptionTrace<114>));
				eSources.Get(115)->FindDeviceEnergyModels(
						"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
						"TotalEnergyConsumption",
						MakeCallback(&TotalEnergyConsumptionTrace<115>));
				eSources.Get(116)->FindDeviceEnergyModels(
						"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
						"TotalEnergyConsumption",
						MakeCallback(&TotalEnergyConsumptionTrace<116>));
				eSources.Get(117)->FindDeviceEnergyModels(
						"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
						"TotalEnergyConsumption",
						MakeCallback(&TotalEnergyConsumptionTrace<117>));
				eSources.Get(118)->FindDeviceEnergyModels(
						"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
						"TotalEnergyConsumption",
						MakeCallback(&TotalEnergyConsumptionTrace<118>));
				eSources.Get(119)->FindDeviceEnergyModels(
						"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
						"TotalEnergyConsumption",
						MakeCallback(&TotalEnergyConsumptionTrace<119>));
				eSources.Get(119)->FindDeviceEnergyModels(
						"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
						"TotalEnergyConsumption",
						MakeCallback(&TotalEnergyConsumptionTrace<119>));
				eSources.Get(119)->FindDeviceEnergyModels(
						"ns3::WifiRadioEnergyModel").Get(0)->TraceConnectWithoutContext(
						"TotalEnergyConsumption",
						MakeCallback(&TotalEnergyConsumptionTrace<119>));
			}
		}
	}
	////////////////
	/*ndn::L3RateTracer::InstallAll("results" + folder + "rate-trace.txt",
	 Minutes(1.0));
	 L2RateTracer::InstallAll("results" + folder + "drop-trace.txt",
	 Seconds(0.5));
	 ndn::AppDelayTracer::InstallAll(
	 "results" + folder + "app-delays-trace.txt");*/

//	wifiPhy.EnablePcap("results/", devices);
//	AsciiTraceHelper ascii;
	//wifiPhy.EnableAsciiAll(ascii.CreateFileStream("wifitrace.tr"));
	//Simulator::Stop(Seconds(3500));
	Simulator::Run();
	Simulator::Destroy();

	return 0;
}

} // namespace ns3

int main(int argc, char* argv[]) {
	return ns3::main(argc, argv);
}
