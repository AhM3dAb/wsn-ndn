#include "wsn-fw-strategy.hpp"
#include <boost/lexical_cast.hpp>
#include <math.h>
#include <ndn-cxx/data.hpp>
#include <ndn-cxx/interest.hpp>
#include <ndn-cxx/name.hpp>
#include <algorithm>
#include <cstdbool>
#include <cstdint>
#include <functional>
#include <iostream>
#include <iterator>
#include <map>
#include <memory>
#include <string>
#include <vector>
#include "ns3/ndnSIM/NFD/core/logger.hpp"
#include "ns3/core-module.h"
#include "ns3/mobility-module.h"
#include "ns3/ndnSIM/NFD/daemon/table/fib-entry.hpp"
#include "ns3/ndnSIM/NFD/daemon/table/fib-nexthop.hpp"
#include "ns3/ndnSIM/NFD/daemon/table/pit-entry.hpp"
#include "ns3/network-module.h"
#include "ndn-consumer.hpp"

namespace nfd {
NFD_LOG_INIT("WsnStrategy");

namespace fw {

const Name WsnStrategy::STRATEGY_NAME(
		"ndn:/localhost/nfd/strategy/wsn-strategy");

WsnStrategy::WsnStrategy(Forwarder& forwarder, const Name& name) :
		Strategy(forwarder, name) {
}
WsnStrategy::~WsnStrategy() {
}
static inline bool predicate_PitEntry_canForwardTo_NextHop(
		shared_ptr<pit::Entry> pitEntry, const fib::NextHop& nexthop) {
	return pitEntry->canForwardTo(*nexthop.getFace());
}
std::string ReplaceString(std::string subject, const std::string& search,
		const std::string& replace) {
	size_t pos = 0;
	while ((pos = subject.find(search, pos)) != std::string::npos) {
		subject.replace(pos, search.length(), replace);
		pos += replace.length();
	}
	return subject;
}
void RoutingTrace(uint32_t nodeId, uint32_t nexthopId, double rEnergy,
		double realRenergy, double distanceToProd, double Threshold,
		double alpha) {
	std::stringstream ss;
	ss << "results/energy_" << nodeId << ".log";
	NFD_LOG_DEBUG(
			ns3::Simulator::Now().GetSeconds() << "\n		NodeId=               " << nodeId << "\n		nextHopId=            "<< nexthopId << "\n		Remaining Energy=     "<< rEnergy << "\n		Real R.Energy=        "<<realRenergy<< "\n		distance to producer= "<<distanceToProd<< "\n		Threshold=            "<<Threshold<< "\n		Alpha=                "<<alpha<< "\n***************************************************************************************");
//  static std::fstream f (ss.str().c_str(), std::ios::out);
//
// // NS_LOG_INFO(Simulator::Now().GetSeconds() << "    remaining energy@ node " << node <<"= " << newValue);
//  f << ns3::Simulator::Now().GetSeconds() << "\n		NodeId=               " << nodeId <<
//		  	  	  	  	  	  	  	  	  	 "\n		nextHopId=            "<< nexthopId <<
//											 "\n		Remaining Energy=     "<< rEnergy <<
//											 "\n		Real R.Energy=        "<<realRenergy<<
//											 "\n		distance to producer= "<<distanceToProd<<
//											 "\n		Threshold=            "<<Threshold<<
//											 "\n		Alpha=                "<<alpha<<
//											 "\n***************************************************************************************"<<
//											 std::endl;

}
void WsnStrategy::afterReceiveInterest(const Face& inFace,
		const Interest& interest, shared_ptr<fib::Entry> fibEntry,
		shared_ptr<pit::Entry> pitEntry) {
	uint32_t nSinkId = 0;
	uint32_t nNodes = ns3::NodeList::GetNNodes();

	if (nNodes == 64)
		nSinkId = 36;
	if (nNodes == 81)
		nSinkId = 40;
	if (nNodes == 100)
		nSinkId = 45;
	if (nNodes == 121)
		nSinkId = 60;
	double xp, yp, zp = 0;
	if (interest.getType() == 'F') {
		xp = boost::lexical_cast<double>(
				ReplaceString(interest.getName().at(-4).toUri(), "%2C", "."));
		yp = boost::lexical_cast<double>(
				ReplaceString(interest.getName().at(-3).toUri(), "%2C", "."));
		zp = boost::lexical_cast<double>(
				ReplaceString(interest.getName().at(-2).toUri(), "%2C", "."));


		NFD_LOG_TRACE("Producer Coordinates: "<< xp << "," << yp << "," << zp );
	}
	double initialEnergy =5; // joule

	uint32_t hopcount = 0;
	uint32_t hopid = 0;
	uint32_t thisNode =
			ns3::NodeList::GetNode(ns3::Simulator::GetContext())->GetId(); //get node id from strategy*/
	ns3::Ptr<ns3::Node> node = ns3::NodeList::GetNode(
			ns3::Simulator::GetContext()); //get node id from strategy*/
	//NFD_LOG_TRACE("afterReceiveInterest"<< pitEntry->getInterest());

	/***************************************************************************
	 *\brief																	   *
	 *send an interest packet of type N with 1 hop limit every node that receive*
	 *this packet save the :													   *
	 *-sender id																   *
	 *-sender coordinate (x,y,z)												   *
	 *-sender remaining energy												   *
	 ****************************************************************************/
	if (interest.getType() == 'N' || interest.getType() == 'X') { //Neighbor Dicovery
	//extract NDP data from interest name
		uint32_t senderId = interest.getSenderId();		//sender id
		double x, y, z, energy;		//Sender x,y coordinate & remaining energy
		x = boost::lexical_cast<double>(
				ReplaceString(interest.getName().at(1).toUri(), "%2C", "."));
		y = boost::lexical_cast<double>(
				ReplaceString(interest.getName().at(2).toUri(), "%2C", "."));
		z = boost::lexical_cast<double>(
				ReplaceString(interest.getName().at(3).toUri(), "%2C", "."));
		energy = interest.getREnergy();
		double thisNodeXcoord, thisNodeYcoord, thisNodeZcoord;
		ns3::Ptr<ns3::Node> node = ns3::NodeList::GetNode(
				ns3::Simulator::GetContext()); //get node id from strategy*/
		ns3::Ptr<ns3::MobilityModel> mob =
				node->GetObject<ns3::MobilityModel>();
		ns3::Vector poss = mob->GetPosition();
		ns3::Ptr<ns3::Node> nodes = ns3::NodeList::GetNode(nSinkId); //get node id from strategy*/
		ns3::Ptr<ns3::MobilityModel> mobs =
				nodes->GetObject<ns3::MobilityModel>();
		ns3::Vector pos = mobs->GetPosition();
		thisNodeXcoord = poss.x;
		thisNodeYcoord = poss.y;
		thisNodeZcoord = poss.z;
		hopcount = boost::lexical_cast<uint32_t>(
				pitEntry->getInterest().getName().at(-1));
		hopcount--;
		if (interest.getType() == 'N') {
			std::string oh = pitEntry->getInterest().getName().at(0).toUri()
					+ "/" + pitEntry->getInterest().getName().at(1).toUri()
					+ "/" + pitEntry->getInterest().getName().at(2).toUri()
					+ "/" + pitEntry->getInterest().getName().at(3).toUri()
					+ "/" + std::to_string(hopcount);
			Name aux(oh);
			const_cast<Interest&>(pitEntry->getInterest()).setName(aux);
		} else {
			std::string oh = pitEntry->getInterest().getName().at(0).toUri()
					+ "/" + std::to_string(thisNodeXcoord) + "/"
					+ std::to_string(thisNodeYcoord) + "/"
					+ std::to_string(thisNodeZcoord) + "/"
					+ std::to_string(hopcount);
			Name aux(oh);
			const_cast<Interest&>(pitEntry->getInterest()).setName(aux);
		}
		double distance = sqrt(
				pow((thisNodeXcoord - x), 2) + pow((thisNodeYcoord - y), 2)
						+ pow((thisNodeZcoord - z), 2));
		if (distance <= 100 && node->GetId() != senderId) {

			node->SetNeighbour(senderId, energy, x, y, z);
		}
	}
	/***************************************************************************/
	else {
		if (node->GetNeighbour().size() < 1) {
			/******************************************************************************
			 * if the senderid doesn't exist on the neighbors table send an NDP packet	  *
			 ******************************************************************************/
			(node->GetApplication(0)->GetObject<ns3::ndn::SConsumer>())->SendPacket();
		}

		else if (node->GetNeighbour().size() > 0) {

			ns3::Ptr<ns3::Node> sinkNode = ns3::NodeList::GetNode(nSinkId); //get node id from strategy*/
			ns3::Ptr<ns3::MobilityModel> mobs = sinkNode->GetObject<
					ns3::MobilityModel>();
			ns3::Vector pos = mobs->GetPosition();
			double thisNodeXcoord, thisNodeYcoord, thisNodeZcoord;
			ns3::Ptr<ns3::Node> node = ns3::NodeList::GetNode(
					ns3::Simulator::GetContext()); //get node id from strategy*/
			ns3::Ptr<ns3::MobilityModel> mob = node->GetObject<
					ns3::MobilityModel>();
			ns3::Vector poss = mob->GetPosition();
			thisNodeXcoord = poss.x;
			thisNodeYcoord = poss.y;
			thisNodeZcoord = poss.z;

			//NFD_LOG_TRACE("Neighbour size: "<< node->GetNeighbour().size());

			/******************************************************************************
			 * if the recived interest is from a neighbour node extract rEnergy 		  *
			 * and update the neighbour table 											  *
			 ******************************************************************************/
			if (interest.getNextHopNId() != 666
					&& interest.getSenderId() != 666) {

				uint32_t exHopId = interest.getSenderId();
				//NFD_LOG_TRACE("1-senderId: "<< exHopId);
				double exRenergy = interest.getREnergy();
				if (node->GetNeighbour().find(exHopId)
						!= node->GetNeighbour().end()) {
					NFD_LOG_TRACE("1-senderId: "<< exHopId);
					node->SetNeighbour(exHopId, exRenergy,
							node->GetNeighbour()[exHopId].x,
							node->GetNeighbour()[exHopId].y,
							node->GetNeighbour()[exHopId].z);
				}
			}

			/*******************************************************************************/
			double distToProd = 0;
			double xx, yy, zz = 0;
			double distanceToProd = 0;
			double alpha = 0.3;
			double thisNodeDistance = 0; //this node distance to producer or sink
			uint32_t nextHopId=666;
			double rEnergy=0;
			double threshold = 10000000;

			if (interest.getType() == 'F') {
				thisNodeDistance = sqrt(
						pow((thisNodeXcoord - xp), 2)
								+ pow((thisNodeYcoord - yp), 2)
								+ pow((thisNodeZcoord - zp), 2));

			} else {
				thisNodeDistance = sqrt(
						pow((thisNodeXcoord - pos.x), 2)
								+ pow((thisNodeYcoord - pos.y), 2)
								+ pow((thisNodeZcoord - pos.z), 2));
			}

			if(thisNodeDistance<=100){
				if (interest.getType() == 'F')
				hopid =boost::lexical_cast<uint32_t>(interest.getName().at(1).toUri());
				else
				hopid = nSinkId;

				NFD_LOG_DEBUG("HOP ID: "<< hopid);
			}
			else{
			for(auto const &ne : node->GetNeighbour()){
			 xx = ne.second.x;
			 yy = ne.second.y;
			 zz = ne.second.z;
			 if (interest.getType() == 'F') {
			 				distanceToProd = sqrt(
			 						pow((xp - xx), 2) + pow((yp - yy), 2)
			 								+ pow((zp - zz), 2));

			 				threshold = alpha * distanceToProd + (1 - alpha) * (initialEnergy-rEnergy);

			 			} else {
			 				distanceToProd = sqrt(
			 						pow((pos.x - xx), 2) + pow((pos.y - yy), 2)
			 								+ pow((pos.z - zz), 2));
			 				threshold = alpha * distanceToProd + (1 - alpha) * (initialEnergy-rEnergy);

			 			}

				if (thisNodeDistance > distToProd) {
					 nextHopId = ne.first;
					 rEnergy =ne.second.energy;
					break;
				}
			}

//			 nextHopId = node->GetNeighbour().begin()->first;
//			 rEnergy =node->GetNeighbour().begin()->second.energy;
//					//ns3::NodeList::GetNode(nextHopId)->GetRemainingEnergy(); //
//			xx = node->GetNeighbour().begin()->second.x;
//			yy = node->GetNeighbour().begin()->second.y;
//			zz = node->GetNeighbour().begin()->second.z;
//
//			double distSP =sqrt(
//										  pow((thisNodeXcoord - pos.x), 2)
//										+ pow((thisNodeYcoord - pos.y), 2)
//										+ pow((thisNodeZcoord - pos.z), 2));
//
//
//			if (interest.getType() == 'F') {
//				distanceToProd = sqrt(
//						pow((xp - xx), 2) + pow((yp - yy), 2)
//								+ pow((zp - zz), 2));
//
//				threshold = alpha * distanceToProd + (1 - alpha) * (rEnergy);
//
//			} else {
//				distanceToProd = sqrt(
//						pow((pos.x - xx), 2) + pow((pos.y - yy), 2)
//								+ pow((pos.z - zz), 2));
//				threshold = alpha * distanceToProd + (1 - alpha) * (rEnergy);
//
//			}
//			NFD_LOG_DEBUG("Node SIZE: "<<node->GetNeighbour().size());


				for(auto const &ne : node->GetNeighbour()){

				//NFD_LOG_DEBUG("Node "<< thisNode << " Neighbour : "<< ne.first);
				xx = ne.second.x;
				yy = ne.second.y;
				zz = ne.second.z;
				if (interest.getType() == 'F') {
					distToProd = sqrt(
							pow((xp - xx), 2) + pow((yp - yy), 2)
									+ pow((zp - zz), 2));
				} else {
					distToProd = sqrt(
							pow((pos.x - xx), 2) + pow((pos.y - yy), 2)
									+ pow((pos.z - zz), 2));
				}


					//NFD_LOG_DEBUG(thisNode<<","<<ne.first<<","<<ne.second.energy<<","<<ns3::NodeList::GetNode(ne.first)->GetRemainingEnergy()<<","<<distToProd<<","<<alpha * distToProd + (1-alpha)* ne.second.energy<<","<<alpha);
					//RoutingTrace(thisNode,ne.first,ne.second.energy,ns3::NodeList::GetNode(ne.first)->GetRemainingEnergy(),distToProd,alpha * distToProd + (1-alpha)* ne.second.energy,alpha);

//					NFD_LOG_DEBUG(
//							ns3::Simulator::Now().GetSeconds()
//							<< "\n		NodeId=               "
//							<< thisNode << "\n		nextHopId=            "
//							<< ne.first << "\n		Remaining Energy=     "
//							<< ne.second.energy << "\n		Real R.Energy=        "
//							<<ns3::NodeList::GetNode(ne.first)->GetRemainingEnergy()
//							<< "\n		distance to producer= "
//							<<distToProd<< "\n		Threshold=            "
//							<<alpha * distToProd + (1-alpha)* initialEnergy-ne.second.energy<< "\n		Alpha=                "
//							<<alpha<< "\n***************************************************************************************");
					if (ne.second.flag != 2 ) {
						if (thisNodeDistance > distToProd) {
							//NFD_LOG_DEBUG("Node distance to sink"<<distToProd);
							if (alpha * distToProd + (1 - alpha) * ( initialEnergy-ne.second.energy) < threshold) {
								threshold = alpha * distToProd
										+ (1 - alpha) * ( initialEnergy-ne.second.energy);
								nextHopId = ne.first;
								distanceToProd = distToProd;
								rEnergy = ne.second.energy;
										//ns3::NodeList::GetNode(ne.first)->GetRemainingEnergy();//
							}
						}
					}
			}
			hopid = nextHopId;
		}
	}
	}
	if (pitEntry->hasUnexpiredOutRecords()) {
		NFD_LOG_DEBUG(
				"beforeExpirePendingInterest pitEntry=" << pitEntry->getName());
		return;
	}
	const fib::NextHopList& nexthops = fibEntry->getNextHops();
	fib::NextHopList::const_iterator it = std::find_if(nexthops.begin(),
			nexthops.end(),
			bind(&predicate_PitEntry_canForwardTo_NextHop, pitEntry, _1));

	shared_ptr<nfd::Face> outFace;
	if (it == nexthops.end()) {
		outFace = const_pointer_cast<Face>(inFace.shared_from_this());
	} else
		outFace = it->getFace();

	if ((thisNode == interest.getNextHopNId() || interest.getNextHopNId() == 666)
			&& (interest.getType() != 'N' && interest.getType() != 'X')) { //first packet sent
		const_cast<Interest&>(pitEntry->getInterest()).setNextHopNId(hopid);
		const_cast<Interest&>(pitEntry->getInterest()).setSenderId(thisNode);
		NFD_LOG_TRACE("interest: "<< pitEntry->getInterest());
		this->sendInterest(pitEntry, outFace);
	} else if (hopcount != 0
			&& (interest.getType() == 'N' || interest.getType() == 'X')) {
		if (interest.getType() == 'N')
			this->sendInterest(pitEntry, outFace);
		else {
			const_cast<Interest&>(pitEntry->getInterest()).setSenderId(
					thisNode);
			const_cast<Interest&>(pitEntry->getInterest()).setREnergy(
					node->GetRemainingEnergy());
			NFD_LOG_TRACE("NDP interest: "<< pitEntry->getInterest());
			this->sendInterest(pitEntry, outFace);
		}
	}
	else
		this->rejectPendingInterest(pitEntry);
}
void WsnStrategy::beforeExpirePendingInterest(shared_ptr<pit::Entry> pitEntry) {
	NFD_LOG_DEBUG(
			"beforeExpirePendingInterest pitEntry=" << pitEntry->getName());
	if (pitEntry->getInterest().getType() != 'N'
			&& pitEntry->getInterest().getType() != 'X') {
		ns3::Ptr<ns3::Node> node = ns3::NodeList::GetNode(
				ns3::Simulator::GetContext());
		if (node->GetNeighbour()[pitEntry->getInterest().getNextHopNId()].flag
				!= 1)
			node->updateNeighbour(pitEntry->getInterest().getNextHopNId(), 2);
	}
}
void WsnStrategy::beforeSatisfyInterest(shared_ptr<pit::Entry> pitEntry,
		const Face& inFace, const ndn::Data& data) {
	NFD_LOG_DEBUG("beforesatisfyInterest pitEntry=" << pitEntry->getName());
}

} // namespace fw
} // namespace nfd
