#ifndef NDNSIM_EXAMPLES_NDN_WSN_STRATEGY_HPP
#define NDNSIM_EXAMPLES_NDN_WSN_STRATEGY_HPP

#include <boost/random/mersenne_twister.hpp>
#include "face/face.hpp"
#include "fw/strategy.hpp"

namespace nfd {
namespace fw {

class WsnStrategy : public Strategy {
public:
	WsnStrategy(Forwarder& forwarder, const Name& name = STRATEGY_NAME);

  virtual ~WsnStrategy();

  virtual void
  afterReceiveInterest(const Face& inFace, const Interest& interest,
                       shared_ptr<fib::Entry> fibEntry, shared_ptr<pit::Entry> pitEntry);

  virtual void
  beforeExpirePendingInterest(shared_ptr<pit::Entry> pitEntry);

  virtual void beforeSatisfyInterest(shared_ptr<pit::Entry> pitEntry,
                                      const Face& inFace, const Data& data);
  uint32_t nextHopNode();
public:
  static const Name STRATEGY_NAME;

protected:
  boost::random::mt19937 m_randomGenerator;
  const FaceId FACEID_CUSTOM_IN = 258;
  const FaceId FACEID_CUSTOM_OUT = 256;

};

} // namespace fw
} // namespace nfd

#endif
