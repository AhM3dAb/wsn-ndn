/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2011-2015  Regents of the University of California.
 *
 * This file is part of ndnSIM. See AUTHORS for complete list of ndnSIM authors and
 * contributors.
 *
 * ndnSIM is free software: you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * ndnSIM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ndnSIM, e.g., in COPYING.md file.  If not, see <http://www.gnu.org/licenses/>.
 **/

// Sink.cpp
#include "sink.hpp"

#include <boost/chrono/duration.hpp>
#include <ndn-cxx/interest.hpp>
#include <boost/lexical_cast.hpp>

#include "ns3/log.h"
#include "ns3/ndnSIM/helper/ndn-fib-helper.hpp"
#include "ns3/ndnSIM/helper/ndn-stack-helper.hpp"
#include "ns3/ptr.h"
#include "ns3/string.h"
#include<string>


NS_LOG_COMPONENT_DEFINE("ndn.Sink");
using namespace std;
namespace ns3 {

// Necessary if you are planning to use ndn::AppHelper
NS_OBJECT_ENSURE_REGISTERED(Sink);

TypeId Sink::GetTypeId() {
	static TypeId tid =
			TypeId("Sink").SetGroupName("Ndn").SetParent<ndn::App>().AddConstructor<
					Sink>().AddAttribute("Prefix",
					"Prefix, wich consumer request", StringValue("/"),
					MakeNameAccessor(&Sink::m_prefix), ndn::MakeNameChecker());

	return tid;
}

Sink::Sink() {
}
std::string ReplaceString(std::string subject, const std::string& search,
                          const std::string& replace) {
    size_t pos = 0;
    while ((pos = subject.find(search, pos)) != std::string::npos) {
         subject.replace(pos, search.length(), replace);
         pos += replace.length();
    }
    return subject;
}
// Callback that will be called when Interest arrives
void Sink::OnInterest(std::shared_ptr<const ndn::Interest> interest) {
	ndn::App::OnInterest(interest);
	auto data = std::make_shared < ndn::Data > (interest->getName());

	if (interest->getType() == 'P') {
		NS_LOG_DEBUG(
				"Received PUSH Interest packet for " << interest->getName());
		ns3::Ptr<ns3::ndn::ContentStore> m_csFromNdnSim;
		//NS_LOG_DEBUG(
			//	"Interest Packet contain DATA: { Location: "<<interest->getName().at(-4)<< ", "<<interest->getName().at(-2)<< ": "<<interest->getName().at(-1)<<"}");

		data->setFreshnessPeriod(ndn::time::milliseconds(1000));
		data->setContent(std::make_shared < ::ndn::Buffer > (0));
		//ndn::StackHelper::getKeyChain().sign(*data);
		NS_LOG_INFO(
				"node(" << GetNode()->GetId() << ") responding with ACK Data Packet");

	}
	else if(interest->getType() == 'M') {
		uint32_t senderId= boost::lexical_cast<double>(ReplaceString(interest->getName().at(1).toUri(),"%2C","."));
		double x=boost::lexical_cast<double>(ReplaceString(interest->getName().at(2).toUri(),"%2C","."));
		double y=boost::lexical_cast<double>(ReplaceString(interest->getName().at(3).toUri(),"%2C","."));
		double z=boost::lexical_cast<double>(ReplaceString(interest->getName().at(4).toUri(),"%2C","."));
		std::string prefix = interest->getName().getSubName(5,interest->getName().size()-5).toUri();
		GetNode()->SetProducer(senderId,prefix,x,y,z);
		data->setFreshnessPeriod(ndn::time::milliseconds(1000));
			data->setContent(std::make_shared < ::ndn::Buffer > (0));
			ndn::StackHelper::getKeyChain().signWithSha256(*data);
			NS_LOG_INFO(
					"node(" << GetNode()->GetId() << ") responding with ACK Data Packet");
	}
	else {
		NS_LOG_DEBUG(
				"Received Fetch Interest packet for " << interest->getName());
		data->setFreshnessPeriod(ndn::time::milliseconds(1000));
		data->setContent(make_shared < ::ndn::Buffer > (50));
		ndn::StackHelper::getKeyChain().signWithSha256(*data);
		NS_LOG_INFO(
				"node(" << GetNode()->GetId() << ") responding with Data: " << data->getName());

	}
	// Call trace (for logging purposes)
	//data->wireEncode();
	if(m_face->isUp()){
	m_transmittedDatas(data, this, m_face);
	m_face->onReceiveData(*data);
	}
	else{
		NS_LOG_INFO("Face is down");
	}
}

// Callback that will be called when Data arrives

void Sink::StartApplication() {
	App::StartApplication();
	// equivalent to setting interest filter for "/prefix" prefix
	ndn::FibHelper::AddRoute(GetNode(), m_prefix, m_face, 0);
}

void Sink::StopApplication() {
	App::StopApplication();
}

} // namespace ns3
