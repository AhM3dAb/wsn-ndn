/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2011-2015  Regents of the University of California.
 *
 * This file is part of ndnSIM. See AUTHORS for complete list of ndnSIM authors and
 * contributors.
 *
 * ndnSIM is free software: you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * ndnSIM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ndnSIM, e.g., in COPYING.md file.  If not, see <http://www.gnu.org/licenses/>.
 **/

// custom-app.cpp
#include "consumerpush.hpp"

#include "ns3/ptr.h"
#include "ns3/log.h"
#include "ns3/simulator.h"
#include "ns3/packet.h"
#include "ns3/callback.h"
#include "ns3/string.h"
#include "ns3/boolean.h"
#include "ns3/uinteger.h"
#include "ns3/integer.h"
#include "ns3/double.h"
#include "ns3/ndnSIM/helper/ndn-stack-helper.hpp"
#include "ns3/ndnSIM/helper/ndn-fib-helper.hpp"
#include "ns3/random-variable-stream.h"
#include "ns3/ndnSIM/model/ndn-l3-protocol.hpp"
#include "model/ndn-l3-protocol.hpp"
#include "model/ndn-app-face.hpp"
#include "ns3/energy-module.h"
#include "../../src/mobility/model/mobility-model.h"
#include <vector>
#include <string>
#include <sstream>
NS_LOG_COMPONENT_DEFINE("ndn.ConsumerPush");

namespace ns3 {

NS_OBJECT_ENSURE_REGISTERED(ConsumerPush);

// register NS-3 type
TypeId ConsumerPush::GetTypeId(void) {
	static TypeId tid =
			TypeId("ConsumerPush").SetGroupName("Ndn").SetParent<ndn::App>().AddConstructor<
					ConsumerPush>().AddAttribute("Prefix",
					"Prefix, for which producer has the data", StringValue("/"),
					MakeNameAccessor(&ConsumerPush::m_prefix),
					ndn::MakeNameChecker()).AddAttribute("PrefixP",
					"PrefixP, for which producer get the data",
					StringValue("-"),
					MakeNameAccessor(&ConsumerPush::m_prefix_p),
					ndn::MakeNameChecker());
	return tid;
}

// Processing upon start of the application
void ConsumerPush::StartApplication() {
	// initialize ndn::App
	ndn::App::StartApplication();

	// Add entry to FIB for `/prefix/sub`
	//ndn::FibHelper::AddRoute(GetNode(), "/", m_face, 0);
	//NS_LOG_DEBUG("Face:" << m_face->getId());

	// Schedule send of interests
	//Neighbour Discovery
	Simulator::Schedule(Seconds(GetNode()->GetId() + 1.0),
			&ConsumerPush::SendInterestNDP, this);

	if (GetNode()->GetId() == 0 || GetNode()->GetId() == 5 || GetNode()->GetId() == 12 )
		Simulator::Schedule(Seconds(30.0 + GetNode()->GetId()),
				&ConsumerPush::SendInterestPDP, this);
	if (GetNode()->GetId() == 24){
		Simulator::Schedule(Seconds(60.0),
				&ConsumerPush::SendInterestx, this);
	Simulator::Schedule(Seconds(65.0),
			&ConsumerPush::SendInterestx, this);
	Simulator::Schedule(Seconds(66.0),
			&ConsumerPush::SendInterestx, this);
	Simulator::Schedule(Seconds(67.0),
			&ConsumerPush::SendInterestx, this);
	Simulator::Schedule(Seconds(68.0),
			&ConsumerPush::SendInterestx, this);
	}
	//Simulator::Schedule(Seconds(40.0+GetNode()->GetId()), &ConsumerPush::SendInterest, this);
}

// Processing when application is stopped
void ConsumerPush::StopApplication() {
	// cleanup ndn::App
	ndn::App::StopApplication();
}

void ConsumerPush::SendInterest() {
	double rEnergy = GetNode()->GetRemainingEnergy();
	NS_LOG_DEBUG(
			" remaining energy @ node "<<GetNode()->GetId() <<"= "<< rEnergy);
	//////////////////////////////////////////
	// Sending one Push Interest packet out //
	//////////////////////////////////////////
	uint32_t i = rand() % 100;
	std::string res = m_prefix.toUri() + "/" + std::to_string(i);
	auto interest = std::make_shared<ndn::Interest>(res);
	interest->setSenderId(GetNode()->GetId());
	interest->setType('P');
	interest->setREnergy(rEnergy);
	interest->setNextHopNId(666);
	Ptr<UniformRandomVariable> rand = CreateObject<UniformRandomVariable>();
	interest->setNonce(rand->GetValue(0, std::numeric_limits<uint32_t>::max()));
	interest->setInterestLifetime(ndn::time::seconds(2));
	NS_LOG_DEBUG("Sending Interest packet " << *interest);
	// Call trace (for logging purposes)
	m_transmittedInterests(interest, this, m_face);
	m_face->onReceiveInterest(*interest);
}
void ConsumerPush::SendInterestNDP() {
	//////////////////////////////////////////
	// Sending one NDP Interest packet out  //
	//////////////////////////////////////////
	double rEnergy = GetNode()->GetRemainingEnergy();
	NS_LOG_DEBUG(
			" remaining energy @ node "<<GetNode()->GetId() <<"= "<< rEnergy);

	Ptr<MobilityModel> mobs = GetNode()->GetObject<MobilityModel>();
	Vector poss = mobs->GetPosition();
	std::string res = "/ndp/"
			+ std::to_string(poss.x) + "/" + std::to_string(poss.y) + "/"+std::to_string(poss.z) + "/"
			+ std::to_string(0);

	auto interest = std::make_shared<ndn::Interest>(res);
	interest->setSenderId(GetNode()->GetId());
	interest->setREnergy(rEnergy);
	interest->setNextHopNId(666);
	interest->setType('N');
	Ptr<UniformRandomVariable> rand = CreateObject<UniformRandomVariable>();
	interest->setNonce(rand->GetValue(0, std::numeric_limits<uint32_t>::max()));
	interest->setInterestLifetime(ndn::time::seconds(2));
	NS_LOG_DEBUG("Sending Interest packet " << *interest);
	// Call trace (for logging purposes)
	m_transmittedInterests(interest, this, m_face);
	m_face->onReceiveInterest(*interest);

}

void ConsumerPush::SendInterestPDP() {

	//////////////////////////////////////////
	// Sending one PDP Interest packet out  //
	//////////////////////////////////////////
	double rEnergy = GetNode()->GetRemainingEnergy();
	NS_LOG_DEBUG(
			" remaining energy @ node "<<GetNode()->GetId() <<"= "<< rEnergy);
	Ptr<MobilityModel> mobs = GetNode()->GetObject<MobilityModel>();
	Vector poss = mobs->GetPosition();
	std::string res = "/sink/"+std::to_string(GetNode()->GetId())+"/"
			+ std::to_string(poss.x) + "/" + std::to_string(poss.y) + "/"
			+ std::to_string(poss.z) + m_prefix_p.toUri();
	auto interest = std::make_shared<ndn::Interest>(res);
	interest->setSenderId(GetNode()->GetId());
	interest->setREnergy(rEnergy);
	interest->setNextHopNId(666);
	interest->setType('M');
	Ptr<UniformRandomVariable> rand = CreateObject<UniformRandomVariable>();
	interest->setNonce(rand->GetValue(0, std::numeric_limits<uint32_t>::max()));
	interest->setInterestLifetime(ndn::time::seconds(2));
	NS_LOG_DEBUG("Sending Interest packet " << *interest);
	// Call trace (for logging purposes)
	m_transmittedInterests(interest, this, m_face);
	m_face->onReceiveInterest(*interest);
}

void ConsumerPush::SendInterestx() {
	///////////////////////////////////////////
	// Sending one Fetch Interest packet out //
	//////////////////////////////////////////
	double rEnergy = GetNode()->GetRemainingEnergy();
		NS_LOG_DEBUG(
				" remaining energy @ node "<<GetNode()->GetId() <<"= "<< rEnergy);
		std::string prefix = "/room0";
		std::string res    = "";
		//////////////////////////////////////////
		// Sending one Push Interest packet out //
		//////////////////////////////////////////
		//uint32_t i = rand() % 100;
		//std::string res = m_prefix.toUri() + "/" + std::to_string(i);
     	for(auto pr =GetNode()->GetProducer().begin();pr!=GetNode()->GetProducer().end();pr++){
     		NS_LOG_DEBUG("prefix= "<<pr->second.prefix<<"---"<<pr->second.x<<","<<pr->second.y<<","<<pr->second.z);
     		if(pr->second.prefix==prefix){
     			res = pr->second.prefix+"/"+std::to_string(pr->first)+"/"+std::to_string(pr->second.x)+"/"+std::to_string(pr->second.y)+"/"+std::to_string(pr->second.z);
     		}
     	}
		auto interest = std::make_shared<ndn::Interest>(res);
		interest->setSenderId(GetNode()->GetId());
		interest->setType('F');
		interest->setREnergy(rEnergy);
		interest->setNextHopNId(666);
		Ptr<UniformRandomVariable> rand = CreateObject<UniformRandomVariable>();
		interest->setNonce(rand->GetValue(0, std::numeric_limits<uint32_t>::max()));
		interest->setInterestLifetime(ndn::time::seconds(2));
		NS_LOG_DEBUG("Sending Interest packet " << *interest);
		// Call trace (for logging purposes)
		m_transmittedInterests(interest, this, m_face);
		m_face->onReceiveInterest(*interest);
}
// Callback that will be called when Interest arrives
//void
//ConsumerPush::OnInterest(std::shared_ptr<const ndn::Interest> interest)
//{
//  ndn::App::OnInterest(interest);
//
//  NS_LOG_DEBUG("Received Interest packet for " << interest->getName());
//  auto data = std::make_shared<ndn::Data>(interest->getName());
//
//  if(interest->getType()=='P'){
//	  std::cout << "Interest packet name component " << interest->getName().at(1);
//	  //NS_LOG_DEBUG("Interest Packet contain DATA: "<< interest->getName().)
//	  data->setFreshnessPeriod(ndn::time::milliseconds(1000));
//	  data->setContent(std::make_shared< ::ndn::Buffer>(0));
//	  ndn::StackHelper::getKeyChain().sign(*data);
//  }
//  else{
//
//  // Note that Interests send out by the app will not be sent back to the app !
//
//  data->setFreshnessPeriod(ndn::time::milliseconds(1000));
//  data->setContent(std::make_shared< ::ndn::Buffer>(1024));
//  ndn::StackHelper::getKeyChain().sign(*data);
//  }
//  NS_LOG_DEBUG("Sending Data packet for " << data->getName());
//
//  // Call trace (for logging purposes)
//  m_transmittedDatas(data, this, m_face);
//
//  m_face->onReceiveData(*data);
//  m_transmittedInterests(interest, this, m_face);
//   m_face->onReceiveInterest(*interest);
//}

// Callback that will be called when Data arrives
void ConsumerPush::OnData(std::shared_ptr<const ndn::Data> data) {
	NS_LOG_DEBUG("Receiving Data packet for " << data->getName());

	//std::cout << "DATA received for name " << data->getName() << std::endl;
}

} // namespace ns3
