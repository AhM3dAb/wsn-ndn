/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2011-2015  Regents of the University of California.
 *
 * This file is part of ndnSIM. See AUTHORS for complete list of ndnSIM authors and
 * contributors.
 *
 * ndnSIM is free software: you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * ndnSIM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ndnSIM, e.g., in COPYING.md file.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "ndn-consumer.hpp"
#include "ns3/core-module.h"
#include "utils/ndn-ns3-packet-tag.hpp"
#include "model/ndn-app-face.hpp"
#include "utils/ndn-rtt-mean-deviation.hpp"
#include "ns3/ndnSIM/helper/ndn-stack-helper.hpp"
#include "ns3/ndnSIM/helper/ndn-fib-helper.hpp"
#include "ns3/ndnSIM/model/ndn-l3-protocol.hpp"
#include "model/ndn-l3-protocol.hpp"
#include "model/ndn-app-face.hpp"
#include "ns3/energy-module.h"
#include "ns3/mobility-module.h"
#include <vector>
#include <string>
#include <sstream>
#include <ctime>
#include <iostream>
#include <random>
#include "ns3/network-module.h"
#include <boost/lexical_cast.hpp>
#include <boost/ref.hpp>

NS_LOG_COMPONENT_DEFINE("ndn.SConsumer");

namespace ns3 {
namespace ndn {

NS_OBJECT_ENSURE_REGISTERED(SConsumer);

TypeId
SConsumer::GetTypeId(void)
{
  static TypeId tid =
    TypeId("SConsumer")
      .SetGroupName("Ndn")
      .SetParent<App>()
      .AddAttribute("StartSeq", "Initial sequence number", IntegerValue(0),
                    MakeIntegerAccessor(&SConsumer::m_seq), MakeIntegerChecker<int32_t>())

      .AddAttribute("Prefix", "Name of the Interest", StringValue("/"),
                    MakeNameAccessor(&SConsumer::m_interestName), MakeNameChecker())
      .AddAttribute("LifeTime", "LifeTime for interest packet", StringValue("2s"),
                    MakeTimeAccessor(&SConsumer::m_interestLifeTime), MakeTimeChecker())

      .AddAttribute("RetxTimer",
                    "Timeout defining how frequent retransmission timeouts should be checked",
                    StringValue("50ms"),
                    MakeTimeAccessor(&SConsumer::GetRetxTimer, &SConsumer::SetRetxTimer),
                    MakeTimeChecker())

	  .AddAttribute("PrefixP",
					"PrefixP, for which producer get the data",
					StringValue("-"),
					MakeNameAccessor(&SConsumer::m_prefix_p),
				    ndn::MakeNameChecker())

      .AddTraceSource("LastRetransmittedInterestDataDelay",
                      "Delay between last retransmitted Interest and received Data",
                      MakeTraceSourceAccessor(&SConsumer::m_lastRetransmittedInterestDataDelay),
                      "ns3::ndn::SConsumer::LastRetransmittedInterestDataDelayCallback")

      .AddTraceSource("FirstInterestDataDelay",
                      "Delay between first transmitted Interest and received Data",
                      MakeTraceSourceAccessor(&SConsumer::m_firstInterestDataDelay),
                      "ns3::ndn::SConsumer::FirstInterestDataDelayCallback");

  return tid;
}
uint32_t sinkid;
SConsumer::SConsumer()
  : m_rand(CreateObject<UniformRandomVariable>())
  , m_seq(0)
  , m_seqMax(0) // don't request anything
{
  NS_LOG_FUNCTION_NOARGS();

  m_rtt = CreateObject<RttMeanDeviation>();
}
void SConsumer::SendInterestNDP(uint32_t i) {
	//////////////////////////////////////////
	// Sending one NDP Interest packet out  //
	//////////////////////////////////////////
	double rEnergy = GetNode()->GetRemainingEnergy();
	//NS_LOG_DEBUG(
		//	" remaining energy @ node "<<GetNode()->GetId() <<"= "<< rEnergy);

	Ptr<MobilityModel> mobs = GetNode()->GetObject<MobilityModel>();
	Vector poss = mobs->GetPosition();
	std::string res = "/ndp/"
			+ std::to_string(poss.x) + "/" + std::to_string(poss.y) + "/"+std::to_string(poss.z) + "/"
			+ std::to_string(i);

	auto interest = std::make_shared<ndn::Interest>(res);
	interest->setSenderId(GetNode()->GetId());
	interest->setREnergy(rEnergy);
	interest->setNextHopNId(666);
	uint8_t c = i==1?'N':'X';
	interest->setType(c);
	Ptr<UniformRandomVariable> rand = CreateObject<UniformRandomVariable>();
	interest->setNonce(rand->GetValue(0, std::numeric_limits<uint32_t>::max()));
	interest->setInterestLifetime(ndn::time::seconds(1));
	NS_LOG_DEBUG("Sending Interest packet " << *interest);
	// Call trace (for logging purposes)
	m_transmittedInterests(interest, this, m_face);
	m_face->onReceiveInterest(*interest);

}

void SConsumer::SendInterestPDP() {

	//////////////////////////////////////////
	// Sending one PDP Interest packet out  //
	//////////////////////////////////////////
	double rEnergy = GetNode()->GetRemainingEnergy();
//	NS_LOG_DEBUG(
//			" remaining energy @ node "<<GetNode()->GetId() <<"= "<< rEnergy);
	Ptr<MobilityModel> mobs = GetNode()->GetObject<MobilityModel>();
	Vector poss = mobs->GetPosition();
	std::string res = "/sink/"+std::to_string(GetNode()->GetId())+"/"
			+ std::to_string(poss.x) + "/" + std::to_string(poss.y) + "/"
			+ std::to_string(poss.z) + m_prefix_p.toUri();
	auto interest = std::make_shared<ndn::Interest>(res);
	interest->setSenderId(GetNode()->GetId());
	interest->setREnergy(rEnergy);
	interest->setNextHopNId(666);
	interest->setType('M');
	Ptr<UniformRandomVariable> rand = CreateObject<UniformRandomVariable>();
	interest->setNonce(rand->GetValue(0, std::numeric_limits<uint32_t>::max()));
	interest->setInterestLifetime(ndn::time::seconds(2));
	NS_LOG_DEBUG("Sending Interest packet " << *interest);
	// Call trace (for logging purposes)
	m_transmittedInterests(interest, this, m_face);
	m_face->onReceiveInterest(*interest);
}
void
SConsumer::SetRetxTimer(Time retxTimer)
{
  m_retxTimer = retxTimer;
  if (m_retxEvent.IsRunning()) {
    // m_retxEvent.Cancel (); // cancel any scheduled cleanup events
    Simulator::Remove(m_retxEvent); // slower, but better for memory
  }

  // schedule even with new timeout
  m_retxEvent = Simulator::Schedule(m_retxTimer, &SConsumer::CheckRetxTimeout, this);
}

Time
SConsumer::GetRetxTimer() const
{
  return m_retxTimer;
}

void
SConsumer::CheckRetxTimeout()
{
  Time now = Simulator::Now();

  Time rto = m_rtt->RetransmitTimeout();
  // NS_LOG_DEBUG ("Current RTO: " << rto.ToDouble (Time::S) << "s");

  while (!m_seqTimeouts.empty()) {
    SeqTimeoutsContainer::index<i_timestamp>::type::iterator entry =
      m_seqTimeouts.get<i_timestamp>().begin();
    if (entry->time + rto <= now) // timeout expired?
    {
      uint32_t seqNo = entry->seq;
      m_seqTimeouts.get<i_timestamp>().erase(entry);
      OnTimeout(seqNo);
    }
    else
      break; // nothing else to do. All later packets need not be retransmitted
  }

  m_retxEvent = Simulator::Schedule(m_retxTimer, &SConsumer::CheckRetxTimeout, this);
}

// Application Methods
void
SConsumer::StartApplication() // Called at time specified by Start
{
  NS_LOG_FUNCTION_NOARGS();

  // do base stuff
  App::StartApplication();

	uint32_t nNodes = ns3::NodeList::GetNNodes();
	if (nNodes == 64)
		sinkid = 36;
	if (nNodes == 81)
		sinkid = 40;
	if (nNodes == 100)
		sinkid = 45;
	if (nNodes == 121)
		sinkid = 60;
	Simulator::Schedule(Seconds(GetNode()->GetId()/2 + 0.5),
			&SConsumer::SendInterestNDP,this,2);


	std::string ax = m_interestName.toUri();
	boost::erase_all(ax, "/");
	uint32_t value = atoi(ax.c_str());
	//NS_LOG_DEBUG("value: "<<value);
	switch (value) {
	case 4: {

			if (nNodes == 64)
				if (GetNode()->GetId() == 0 || GetNode()->GetId() == 7 || GetNode()->GetId() == 56 || GetNode()->GetId() == 63) {
					Simulator::Schedule(Seconds(100 + 0.1*GetNode()->GetId()),
							&SConsumer::SendInterestPDP, this);
				}
			if (nNodes == 81)
				if (GetNode()->GetId() == 0 || GetNode()->GetId() == 8 || GetNode()->GetId() == 72 || GetNode()->GetId() == 80) {
					Simulator::Schedule(Seconds(100 + 0.1*GetNode()->GetId()),
							&SConsumer::SendInterestPDP, this);
				}
			if (nNodes == 100)
				if (GetNode()->GetId() == 0 || GetNode()->GetId() == 9 || GetNode()->GetId() == 90 || GetNode()->GetId() == 99) {
					Simulator::Schedule(Seconds(100 + 0.1*GetNode()->GetId()),
							&SConsumer::SendInterestPDP, this);
				}
			if (nNodes == 121)
				if (GetNode()->GetId() == 0 || GetNode()->GetId() == 10 || GetNode()->GetId() == 110 || GetNode()->GetId() == 120) {
					Simulator::Schedule(Seconds(100 + 0.1*GetNode()->GetId()),
							&SConsumer::SendInterestPDP, this);
				}


	}
		break;

	case 8: {

			if (nNodes == 64)
				if (GetNode()->GetId() == 0 || GetNode()->GetId() == 7 || GetNode()->GetId() == 56 || GetNode()->GetId() == 63 || GetNode()->GetId() == 4 || GetNode()->GetId() == 31
						|| GetNode()->GetId() == 24 || GetNode()->GetId() == 60) {
					Simulator::Schedule(Seconds(100 + 0.1*GetNode()->GetId()),
							&SConsumer::SendInterestPDP, this);
				}
			if (nNodes == 81)
				if (GetNode()->GetId() == 0 || GetNode()->GetId() == 8 || GetNode()->GetId() == 72 || GetNode()->GetId() == 80 || GetNode()->GetId() == 76 || GetNode()->GetId() == 36
						|| GetNode()->GetId() == 44 || GetNode()->GetId() == 4) {
					Simulator::Schedule(Seconds(100 + 0.1*GetNode()->GetId()),
							&SConsumer::SendInterestPDP, this);
				}
			if (nNodes == 100)
				if (GetNode()->GetId() == 0 || GetNode()->GetId() == 9 || GetNode()->GetId() == 90 || GetNode()->GetId() == 99 || GetNode()->GetId() == 5 || GetNode()->GetId() == 40
						|| GetNode()->GetId() == 49 || GetNode()->GetId() == 95) {
					Simulator::Schedule(Seconds(100 + 0.1*GetNode()->GetId()),
							&SConsumer::SendInterestPDP, this);
				}
			if (nNodes == 121)
				if (GetNode()->GetId() == 0 || GetNode()->GetId() == 10 || GetNode()->GetId() == 110 || GetNode()->GetId() == 120 || GetNode()->GetId() == 5
						|| GetNode()->GetId() == 55 || GetNode()->GetId() == 65 || GetNode()->GetId() == 115) {
					Simulator::Schedule(Seconds(100 + 0.1*GetNode()->GetId()),
							&SConsumer::SendInterestPDP, this);
				}

	}
		break;
	case 12: {
			if (nNodes == 64)
				if (GetNode()->GetId() == 0 || GetNode()->GetId() == 7 || GetNode()->GetId() == 56 || GetNode()->GetId() == 63 || GetNode()->GetId() == 4 || GetNode()->GetId() == 31
						|| GetNode()->GetId() == 24 || GetNode()->GetId() == 60 || GetNode()->GetId() == 3 || GetNode()->GetId() == 39 || GetNode()->GetId() == 32
						|| GetNode()->GetId() == 59) {
					Simulator::Schedule(Seconds(100 + 0.1*GetNode()->GetId()),
							&SConsumer::SendInterestPDP, this);
				}
			if (nNodes == 81)
				if (GetNode()->GetId() == 0 || GetNode()->GetId() == 8 || GetNode()->GetId() == 72 || GetNode()->GetId() == 80 || GetNode()->GetId() == 76 || GetNode()->GetId() == 36
						|| GetNode()->GetId() == 44 || GetNode()->GetId() == 4 || GetNode()->GetId() == 75 || GetNode()->GetId() == 27 || GetNode()->GetId() == 35
						|| GetNode()->GetId() == 3) {
					Simulator::Schedule(Seconds(100 + 0.1*GetNode()->GetId()),
							&SConsumer::SendInterestPDP, this);
				}
			if (nNodes == 100)
				if (GetNode()->GetId() == 0 || GetNode()->GetId() == 9 || GetNode()->GetId() == 90 || GetNode()->GetId() == 99 || GetNode()->GetId() == 5 || GetNode()->GetId() == 40
						|| GetNode()->GetId() == 49 || GetNode()->GetId() == 95 || GetNode()->GetId() == 4 || GetNode()->GetId() == 30 || GetNode()->GetId() == 39
						|| GetNode()->GetId() == 94) {
					Simulator::Schedule(Seconds(100 + 0.1*GetNode()->GetId()),
							&SConsumer::SendInterestPDP, this);
				}
			if (nNodes == 121)
				if (GetNode()->GetId() == 0 || GetNode()->GetId() == 10 || GetNode()->GetId() == 110 || GetNode()->GetId() == 120 || GetNode()->GetId() == 5
						|| GetNode()->GetId() == 55 || GetNode()->GetId() == 65 || GetNode()->GetId() == 115 || GetNode()->GetId() == 4 || GetNode()->GetId() == 44
						|| GetNode()->GetId() == 54 || GetNode()->GetId() == 114) {
					Simulator::Schedule(Seconds(100 + 0.1*GetNode()->GetId()),
							&SConsumer::SendInterestPDP, this);
				}

	}
		break;
	case 16: {
			if (nNodes == 64)
				if (GetNode()->GetId() == 0 || GetNode()->GetId() == 7 || GetNode()->GetId() == 56 || GetNode()->GetId() == 63 || GetNode()->GetId() == 4 || GetNode()->GetId() == 31
						|| GetNode()->GetId() == 24 || GetNode()->GetId() == 60 || GetNode()->GetId() == 3 || GetNode()->GetId() == 39 || GetNode()->GetId() == 32
						|| GetNode()->GetId() == 59 || GetNode()->GetId() == 5 || GetNode()->GetId() == 23 || GetNode()->GetId() == 16 || GetNode()->GetId() == 61) {
					Simulator::Schedule(Seconds(100 + 0.1*GetNode()->GetId()),
							&SConsumer::SendInterestPDP, this);
				}
			if (nNodes == 81)
				if (GetNode()->GetId() == 0 || GetNode()->GetId() == 8 || GetNode()->GetId() == 72 || GetNode()->GetId() == 80 || GetNode()->GetId() == 76 || GetNode()->GetId() == 36
						|| GetNode()->GetId() == 44 || GetNode()->GetId() == 4 || GetNode()->GetId() == 75 || GetNode()->GetId() == 27 || GetNode()->GetId() == 35
						|| GetNode()->GetId() == 3 || GetNode()->GetId() == 77 || GetNode()->GetId() == 45 || GetNode()->GetId() == 53 || GetNode()->GetId() == 5) {
					Simulator::Schedule(Seconds(100 + 0.1*GetNode()->GetId()),
							&SConsumer::SendInterestPDP, this);
				}
			if (nNodes == 100)
				if (GetNode()->GetId() == 0 || GetNode()->GetId() == 9 || GetNode()->GetId() == 90 || GetNode()->GetId() == 99 || GetNode()->GetId() == 5 || GetNode()->GetId() == 40
						|| GetNode()->GetId() == 49 || GetNode()->GetId() == 95 || GetNode()->GetId() == 4 || GetNode()->GetId() == 30 || GetNode()->GetId() == 39
						|| GetNode()->GetId() == 94 || GetNode()->GetId() == 6 || GetNode()->GetId() == 50 || GetNode()->GetId() == 59 || GetNode()->GetId() == 96) {
					Simulator::Schedule(Seconds(100 + 0.1*GetNode()->GetId()),
							&SConsumer::SendInterestPDP, this);
				}
			if (nNodes == 121)
				if (GetNode()->GetId() == 0 || GetNode()->GetId() == 10 || GetNode()->GetId() == 110 || GetNode()->GetId() == 120 || GetNode()->GetId() == 5
						|| GetNode()->GetId() == 55 || GetNode()->GetId() == 65 || GetNode()->GetId() == 115 || GetNode()->GetId() == 4 || GetNode()->GetId() == 44
						|| GetNode()->GetId() == 54 || GetNode()->GetId() == 114 || GetNode()->GetId() == 6 || GetNode()->GetId() == 66 || GetNode()->GetId() == 76
						|| GetNode()->GetId() == 116) {
					Simulator::Schedule(Seconds(100 + 0.1*GetNode()->GetId()),
							&SConsumer::SendInterestPDP, this);
				}

	}
		break;
	default:{
		if (GetNode()->GetId() == 0)
		Simulator::Schedule(Seconds(100 + 0.1*GetNode()->GetId()),
				&SConsumer::SendInterestPDP, this);
	}
	break;
	}

switch(nNodes){
case 64:{
	if (GetNode()->GetId() == sinkid){
			std::random_device rd;     // only used once to initialise (seed) engine
			std::mt19937 rng(rd());    // random-number engine
			std::uniform_int_distribution<int> uni(0,0.20); // guaranteed unbiased
			auto random_integer = uni(rng);
			//NS_LOG_DEBUG("Delay: "<< 0);
		   double delay = random_integer;
			Simulator::Schedule(Minutes(2+delay), &SConsumer::ScheduleNextPacket,this);
		}

}
break;
case 81:{
	if (GetNode()->GetId() == sinkid){
			std::random_device rd;     // only used once to initialise (seed) engine
			std::mt19937 rng(rd());    // random-number engine
			std::uniform_int_distribution<int> uni(0,0.20); // guaranteed unbiased
			auto random_integer = uni(rng);
			//NS_LOG_DEBUG("Delay: "<< 0);
		   double delay = random_integer;
			Simulator::Schedule(Minutes(2+delay), &SConsumer::ScheduleNextPacket,this);
		}

}
break;
case 100:{
	if (GetNode()->GetId() == sinkid){
			std::random_device rd;     // only used once to initialise (seed) engine
			std::mt19937 rng(rd());    // random-number engine
			std::uniform_int_distribution<int> uni(0,0.20); // guaranteed unbiased
			auto random_integer = uni(rng);
			//NS_LOG_DEBUG("Delay: "<< 0);
		   double delay = random_integer;
			Simulator::Schedule(Minutes(2+delay), &SConsumer::ScheduleNextPacket,this);
		}

}
break;
case 121:{
	if (GetNode()->GetId() == sinkid){
			std::random_device rd;     // only used once to initialise (seed) engine
			std::mt19937 rng(rd());    // random-number engine
			std::uniform_int_distribution<int> uni(0,0.20); // guaranteed unbiased
			auto random_integer = uni(rng);
			//NS_LOG_DEBUG("Delay: "<< 0);
		   double delay = random_integer;
			Simulator::Schedule(Minutes(2+delay), &SConsumer::ScheduleNextPacket,this);
		}

}
break;
}

  //ScheduleNextPacket();
}

void
SConsumer::StopApplication() // Called at time specified by Stop
{
  NS_LOG_FUNCTION_NOARGS();

  // cancel periodic packet generation
  Simulator::Cancel(m_sendEvent);

  // cleanup base stuff
  App::StopApplication();
}



void SConsumer::SendInterest() {
	 if (!m_active)
	    return;

	  NS_LOG_FUNCTION_NOARGS();

	  uint32_t seq = std::numeric_limits<uint32_t>::max(); // invalid

	  while (m_retxSeqs.size()) {
	    seq = *m_retxSeqs.begin();
	    m_retxSeqs.erase(m_retxSeqs.begin());
	    break;
	  }

	  if (seq == std::numeric_limits<uint32_t>::max()) {
	    if (m_seqMax != std::numeric_limits<uint32_t>::max()) {
	      if (m_seq >= m_seqMax) {
	        return; // we are totally done
	      }
	    }

	    seq = m_seq++;
	  }
	double rEnergy = GetNode()->GetRemainingEnergy();
	//NS_LOG_DEBUG(
		//	" remaining energy @ node "<<GetNode()->GetId() <<"= "<< rEnergy);
	//////////////////////////////////////////
	// Sending one Push Interest packet out //
	//////////////////////////////////////////
	shared_ptr<Name> nameWithSequence = make_shared<Name>(m_interestName);
	nameWithSequence->appendSequenceNumber(seq);
	auto interest = std::make_shared<ndn::Interest>(*nameWithSequence);
	interest->setSenderId(GetNode()->GetId());
	interest->setType('P');
	interest->setREnergy(rEnergy);
	interest->setNextHopNId(666);
	Ptr<UniformRandomVariable> rand = CreateObject<UniformRandomVariable>();
	interest->setNonce(rand->GetValue(0, std::numeric_limits<uint32_t>::max()));
	time::milliseconds interestLifeTime(m_interestLifeTime.GetMilliSeconds());
	interest->setInterestLifetime(interestLifeTime);
	NS_LOG_DEBUG("Sending Interest packet " << *interest);
	// Call trace (for logging purposes)
	NS_LOG_INFO("> Interest for " << seq);
	WillSendOutInterest(seq);
	m_transmittedInterests(interest, this, m_face);
	m_face->onReceiveInterest(*interest);
}


void SConsumer::SendInterestx(std::string pref) {
	///////////////////////////////////////////
	// Sending one Fetch Interest packet out //
	//////////////////////////////////////////
	if (!m_active)
	    return;

	  NS_LOG_FUNCTION_NOARGS();

	  uint32_t seq = std::numeric_limits<uint32_t>::max(); // invalid

	  while (m_retxSeqs.size()) {
	    seq = *m_retxSeqs.begin();
	    m_retxSeqs.erase(m_retxSeqs.begin());
	    break;
	  }

	  if (seq == std::numeric_limits<uint32_t>::max()) {
	    if (m_seqMax != std::numeric_limits<uint32_t>::max()) {
	      if (m_seq >= m_seqMax) {
	        return; // we are totally done
	      }
	    }

	    seq = m_seq++;
	  }

	double rEnergy = GetNode()->GetRemainingEnergy();

				std::string prefix = pref;
				std::string res    = "";
				for(auto const &pr :GetNode()->GetProducer()){
				//NS_LOG_DEBUG("prefix= "<<pr.second.prefix<<"---"<<pr.second.x<<","<<pr.second.y<<","<<pr.second.z);
				if(pr.second.prefix==prefix){
					res = pr.second.prefix+"/"+std::to_string(pr.first)+"/"+std::to_string(pr.second.x)+"/"+std::to_string(pr.second.y)+"/"+std::to_string(pr.second.z);
				   }
				}
				shared_ptr<Name> nameWithSequence = make_shared<Name>(res);
				nameWithSequence->appendSequenceNumber(seq);
				auto interest = std::make_shared<ndn::Interest>(*nameWithSequence);
				interest->setSenderId(GetNode()->GetId());
				interest->setType('F');
				interest->setREnergy(rEnergy);
				interest->setNextHopNId(666);
				Ptr<UniformRandomVariable> rand = CreateObject<UniformRandomVariable>();
				interest->setNonce(rand->GetValue(0, std::numeric_limits<uint32_t>::max()));
				time::milliseconds interestLifeTime(m_interestLifeTime.GetMilliSeconds());
				interest->setInterestLifetime(interestLifeTime);
               // NS_LOG_INFO ("Requesting Interest: \n" << *interest);
				NS_LOG_INFO("> Interest for " << seq);
				WillSendOutInterest(seq);
				m_transmittedInterests(interest, this, m_face);
				m_face->onReceiveInterest(*interest);
				//NS_LOG_DEBUG("interest sent= "<< *interest);

}
void
SConsumer::SendPacket()
{
	//NS_LOG_DEBUG("SendPacket************************************************");
	Simulator::Schedule(Seconds(0.5),&SConsumer::SendInterestNDP,this,3);

}
void
SConsumer::SendPacketI(){
	std::string ax = m_interestName.toUri();
	boost::erase_all(ax, "/");
	uint32_t value = atoi(ax.c_str());
	uint32_t nNodes = ns3::NodeList::GetNNodes();
	std::random_device rd;     // only used once to initialise (seed) engine
		std::mt19937 rng(rd());    // random-number engine
		std::uniform_int_distribution<int> uni(0, 2); // guaranteed unbiased
		auto random_integer = uni(rng);
		double delaya = random_integer;
	switch (value) {
		case 4: {
			for (uint32_t i = 0; i <= nNodes; i++) {
				if (nNodes == 64){
					if (i == 0 || i == 7 || i == 56 || i == 63) {
						auto random_integer = uni(rng);
						double delay = random_integer;
						   Simulator::Schedule(Seconds(delay),&SConsumer::SendInterestx,this,"/room"+std::to_string(i));
					}
				}
				else if (nNodes == 81){
					if (i == 0 || i == 8 || i == 72 || i == 80) {
						auto random_integer = uni(rng);
						double delay = random_integer;
						   Simulator::Schedule(Seconds(delay),&SConsumer::SendInterestx,this,"/room"+std::to_string(i));

					}}
				else if (nNodes == 100){
					if (i == 0 || i == 9 || i == 90 || i == 99) {
						auto random_integer = uni(rng);
						double delay = random_integer;
						   Simulator::Schedule(Seconds(delay),&SConsumer::SendInterestx,this,"/room"+std::to_string(i));

					}}
				else if (nNodes == 121){
					if (i == 0 || i == 10 || i == 110 || i == 120){
					auto random_integer = uni(rng);
				double delay = random_integer;
				   Simulator::Schedule(Seconds(delay),&SConsumer::SendInterestx,this,"/room"+std::to_string(i));

			}}}

		}
			break;

		case 8: {

			for (uint32_t i = 0; i < nNodes; i++) {
				if (nNodes == 64){
					if (i == 0 || i == 7 || i == 56 || i == 63 || i == 4 || i == 31
							|| i == 24 || i == 60) {
						auto random_integer = uni(rng);
						double delay = random_integer;
						   Simulator::Schedule(Seconds(delay),&SConsumer::SendInterestx,this,"/room"+std::to_string(i));

					}}
				else if (nNodes == 81){
					if (i == 0 || i == 8 || i == 72 || i == 80 || i == 76 || i == 36
							|| i == 44 || i == 4) {
						auto random_integer = uni(rng);
						double delay = random_integer;
						   Simulator::Schedule(Seconds(delay),&SConsumer::SendInterestx,this,"/room"+std::to_string(i));

					}}
				else if (nNodes == 100){
					if (i == 0 || i == 9 || i == 90 || i == 99 || i == 5 || i == 40
							|| i == 49 || i == 95) {
						auto random_integer = uni(rng);
						double delay = random_integer;
						   Simulator::Schedule(Seconds(delay),&SConsumer::SendInterestx,this,"/room"+std::to_string(i));

					}}
				else if (nNodes == 121){
					if (i == 0 || i == 10 || i == 110 || i == 120 || i == 5
							|| i == 55 || i == 65 || i == 115) {
						auto random_integer = uni(rng);
						double delay = random_integer;
						   Simulator::Schedule(Seconds(delay),&SConsumer::SendInterestx,this,"/room"+std::to_string(i));

					}}
			}
		}
			break;
		case 12: {
			for (uint32_t i = 0; i < nNodes; i++) {
				if (nNodes == 64){
					if (i == 0 || i == 7 || i == 56 || i == 63 || i == 4 || i == 31
							|| i == 24 || i == 60 || i == 3 || i == 39 || i == 32
							|| i == 59) {
						auto random_integer = uni(rng);
						double delay = random_integer;
						   Simulator::Schedule(Seconds(delay),&SConsumer::SendInterestx,this,"/room"+std::to_string(i));

					}}
				else if (nNodes == 81){
					if (i == 0 || i == 8 || i == 72 || i == 80 || i == 76 || i == 36
							|| i == 44 || i == 4 || i == 75 || i == 27 || i == 35
							|| i == 3) {
						auto random_integer = uni(rng);
						double delay = random_integer;
						   Simulator::Schedule(Seconds(delay),&SConsumer::SendInterestx,this,"/room"+std::to_string(i));

					}}
				else if (nNodes == 100){
					if (i == 0 || i == 9 || i == 90 || i == 99 || i == 5 || i == 40
							|| i == 49 || i == 95 || i == 4 || i == 30 || i == 39
							|| i == 94) {
						auto random_integer = uni(rng);
						double delay = random_integer;
						   Simulator::Schedule(Seconds(delay),&SConsumer::SendInterestx,this,"/room"+std::to_string(i));

					}}
				else if (nNodes == 121){
					if (i == 0 || i == 10 || i == 110 || i == 120 || i == 5
							|| i == 55 || i == 65 || i == 115 || i == 4 || i == 44
							|| i == 54 || i == 114) {
						auto random_integer = uni(rng);
						double delay = random_integer;
						   Simulator::Schedule(Seconds(delay),&SConsumer::SendInterestx,this,"/room"+std::to_string(i));

					}}
			}
		}
			break;
		case 16: {
			for (uint32_t i = 0; i < nNodes; i++) {
				if (nNodes == 64){
					if (i == 0 || i == 7 || i == 56 || i == 63 || i == 4 || i == 31
							|| i == 24 || i == 60 || i == 3 || i == 39 || i == 32
							|| i == 59 || i == 5 || i == 23 || i == 16 || i == 61) {
						auto random_integer = uni(rng);
						double delay = random_integer;
						   Simulator::Schedule(Seconds(delay),&SConsumer::SendInterestx,this,"/room"+std::to_string(i));

					}}
				else if (nNodes == 81){
					if (i == 0 || i == 8 || i == 72 || i == 80 || i == 76 || i == 36
							|| i == 44 || i == 4 || i == 75 || i == 27 || i == 35
							|| i == 3 || i == 77 || i == 45 || i == 53 || i == 5) {
						auto random_integer = uni(rng);
						double delay = random_integer;
						   Simulator::Schedule(Seconds(delay),&SConsumer::SendInterestx,this,"/room"+std::to_string(i));

					}}
				else if (nNodes == 100){
					if (i == 0 || i == 9 || i == 90 || i == 99 || i == 5 || i == 40
							|| i == 49 || i == 95 || i == 4 || i == 30 || i == 39
							|| i == 94 || i == 6 || i == 50 || i == 59 || i == 96) {
						auto random_integer = uni(rng);
						double delay = random_integer;
						   Simulator::Schedule(Seconds(delay),&SConsumer::SendInterestx,this,"/room"+std::to_string(i));

					}}
				else if (nNodes == 121){
					if (i == 0 || i == 10 || i == 110 || i == 120 || i == 5
							|| i == 55 || i == 65 || i == 115 || i == 4 || i == 44
							|| i == 54 || i == 114 || i == 6 || i == 66 || i == 76
							|| i == 116) {
						auto random_integer = uni(rng);
						double delay = random_integer;
						   Simulator::Schedule(Seconds(delay),&SConsumer::SendInterestx,this,"/room"+std::to_string(i));

					}}
			}
		}
			break;
		default: {
			   Simulator::Schedule(Seconds(delaya),&SConsumer::SendInterestx,this,"/room"+std::to_string(0));

		}
			break;
		}

	ScheduleNextPacket();
}
///////////////////////////////////////////////////
//          Process incoming packets             //
///////////////////////////////////////////////////

void
SConsumer::OnData(shared_ptr<const Data> data)
{
  if (!m_active)
    return;

  App::OnData(data); // tracing inside

  NS_LOG_FUNCTION(this << data);

  // NS_LOG_INFO ("Received content object: " << boost::cref(*data));

  // This could be a problem......
  if(data->getName().at(0).toUri()=="sink" ){return;}
  uint32_t seq = data->getName().at(-1).toSequenceNumber();
  NS_LOG_INFO("< DATA for " << seq);

  int hopCount = 0;
  auto ns3PacketTag = data->getTag<Ns3PacketTag>();
  if (ns3PacketTag != nullptr) { // e.g., packet came from local node's cache
    FwHopCountTag hopCountTag;
    if (ns3PacketTag->getPacket()->PeekPacketTag(hopCountTag)) {
      hopCount = hopCountTag.Get();
      NS_LOG_DEBUG("Hop count: " << hopCount);
    }
  }

  SeqTimeoutsContainer::iterator entry = m_seqLastDelay.find(seq);
  if (entry != m_seqLastDelay.end()) {
    m_lastRetransmittedInterestDataDelay(this, seq, Simulator::Now() - entry->time, hopCount);
  }

  entry = m_seqFullDelay.find(seq);
  if (entry != m_seqFullDelay.end()) {
    m_firstInterestDataDelay(this, seq, Simulator::Now() - entry->time, m_seqRetxCounts[seq], hopCount);
  }

  m_seqRetxCounts.erase(seq);
  m_seqFullDelay.erase(seq);
  m_seqLastDelay.erase(seq);

  m_seqTimeouts.erase(seq);
  m_retxSeqs.erase(seq);

  m_rtt->AckSeq(SequenceNumber32(seq));
}

void
SConsumer::OnTimeout(uint32_t sequenceNumber)
{
  NS_LOG_FUNCTION(sequenceNumber);
  if(GetNode()->GetId()==sinkid)
  GetNode()->SetNTimeout(GetNode()->GetNTieout()+1);
  // std::cout << Simulator::Now () << ", TO: " << sequenceNumber << ", current RTO: " <<
  // m_rtt->RetransmitTimeout ().ToDouble (Time::S) << "s\n";

  m_rtt->IncreaseMultiplier(); // Double the next RTO
  m_rtt->SentSeq(SequenceNumber32(sequenceNumber),
                 1); // make sure to disable RTT calculation for this sample
  m_retxSeqs.insert(sequenceNumber);
  ScheduleNextPacket();
}

void
SConsumer::WillSendOutInterest(uint32_t sequenceNumber)
{
  NS_LOG_DEBUG("Trying to add " << sequenceNumber << " with " << Simulator::Now() << ". already "
                                << m_seqTimeouts.size() << " items");

  m_seqTimeouts.insert(SeqTimeout(sequenceNumber, Simulator::Now()));
  m_seqFullDelay.insert(SeqTimeout(sequenceNumber, Simulator::Now()));

  m_seqLastDelay.erase(sequenceNumber);
  m_seqLastDelay.insert(SeqTimeout(sequenceNumber, Simulator::Now()));

  m_seqRetxCounts[sequenceNumber]++;

  m_rtt->SentSeq(SequenceNumber32(sequenceNumber), 1);
}

} // namespace ndn
} // namespace ns3
