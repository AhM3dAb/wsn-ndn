#!/bin/bash
cd /home/ahmed/Desktop/Dev/Network-tools/ndnSIMEclipse/ns-3/scenario
for v in 64 81 100 121
do
for (( c=1; c<=20; c++ ))
do
echo "************** 1 TASK --numtask=1 --nNodes=$v --fld='/$v/1/$c/'***********************"
NS_LOG=ndn.Sink:ndn.SConsumer ./waf --run="wsn-wifi --numtask=1 --nNodes=$v --fld=/$v/1/$c/"
sleep 5
echo "************** 4 TASK --numtask=4 --nNodes=$v --fld='/$v/4/$c/'************************"
NS_LOG=ndn.Sink:ndn.SConsumer ./waf --run="wsn-wifi --numtask=4 --nNodes=$v --fld=/$v/4/$c/"
sleep 5
echo "************** 8 TASK --numtask=8 --nNodes=$v --fld='/$v/8/$c/'************************"
NS_LOG=ndn.Sink:ndn.SConsumer ./waf --run="wsn-wifi --numtask=8 --nNodes=$v --fld=/$v/8/$c/"
sleep 5
echo "************** 12 TASK --numtask=12 --nNodes=$v --fld='/$v/12/$c/'************************"
NS_LOG=ndn.Sink:ndn.SConsumer ./waf --run="wsn-wifi --numtask=12 --nNodes=$v --fld=/$v/12/$c/"
sleep 5
echo "************** 16 TASK --numtask=16 --nNodes=$v --fld='/$v/16/$c/'************************"
NS_LOG=ndn.Sink:ndn.SConsumer ./waf --run="wsn-wifi --numtask=16 --nNodes=$v --fld=/$v/16/$c/"
sleep 5
done
done





